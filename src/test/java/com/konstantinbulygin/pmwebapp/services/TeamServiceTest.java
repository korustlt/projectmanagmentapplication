package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.TeamRepository;
import com.konstantinbulygin.pmwebapp.entities.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса TeamService
 */
@SpringBootTest
class TeamServiceTest {

    /**
     * Внедрение зависимости TeamService
     */
    @Autowired
    TeamService teamService;

    /**
     * Внедрение подменной зависимости TeamRepository
     */
    @MockBean
    TeamRepository teamRepository;

    /**
     * Поле Team
     */
    private Team team;

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    public void setUp() {
        this.team = new Team(1L, "TeamName1", new ArrayList<>());
    }

    /**
     * Должен сохранить команду в базу данных
     */
    @Test
    public void shouldSaveTeam() {

        /* создаем подменный репозиторий и указываем что метод  save() должен вернуть сохраненный объект */
        when(teamRepository.save(team)).thenReturn(team);
        /* сохраняем объект team1 */
        Team savedTeam = teamService.save(team);
        /* проверяем что сохранение проходит успешно */
        verify(teamRepository).save(team);
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedTeam).hasFieldOrPropertyWithValue("teamId", team.getTeamId());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedTeam).hasFieldOrPropertyWithValue("teamName", team.getTeamName());
    }

    /**
     * Должен обновить команду по id
     */
    @Test
    public void shouldUpdateTeamById() {
        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(teamRepository.save(team)).thenReturn(team);
        /* сохраняем объект team1 */
        Team savedTeam = teamService.save(team);
        /* создаем новое название команды */
        String newTeamName = "New Team name";
        /* устанавливаем новое значение */
        savedTeam.setTeamName(newTeamName);
        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(teamRepository.save(savedTeam)).thenReturn(savedTeam);
        /* сохраняем объект с обновленным полем */
        Team updatedTeam = teamService.save(savedTeam);
        /* утверждаем что обновленный объект имеет такой же id как и первоначально сохраненный */
        assertThat(updatedTeam.getTeamId()).isEqualTo(savedTeam.getTeamId());
        /* утверждаем что обновленный объект имеет новое описание */
        assertThat(updatedTeam.getTeamName()).isEqualTo(newTeamName);
    }

    /**
     * Должен удалить команду по id
     */
    @Test
    public void shouldDeleteTeamById() {
        /* удаляем объект по id*/
        teamService.delete(team.getTeamId());
        /* проверяем что удаление проходит успешно */
        verify(teamRepository).deleteById(team.getTeamId());
    }

    /**
     * Должен найти все команды
     */
    @Test
    void shouldFindAllTeams() {

        /* создаем подменный репозиторий и указываем что метод findAll() должен вернуть 3 объекта */
        when(teamRepository.findAll()).thenReturn(Arrays.asList(team, team, team));
        /* получаем 3 объекта Team */
        List<Team> teamList = teamService.findAll();
        /* утверждаем что размер списка равен 3 */
        Assertions.assertEquals(3, teamList.size(), "метод findAll() должен вернуть 3 объекта");
    }

    /**
     * Должен найти команду по id
     */
    @Test
    void shouldFindTeamById() {

        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(teamRepository.findById(anyLong())).thenReturn(Optional.of(team));
        /* получаем объект по id */
        Optional<Team> returnedTeam = teamService.findById(1L);
        /* проверяем что поиск проходит успешно */
        verify(teamRepository).findById(1L);
        /* утверждаем что найденный объект есть */
        Assertions.assertTrue(returnedTeam.isPresent(), "Объект не был найден");
        /* утверждаем что найденный объект равен подменному объекту */
        Assertions.assertSame(returnedTeam.get(), team, "Найденный объект не равен подменному объекту");
    }
}