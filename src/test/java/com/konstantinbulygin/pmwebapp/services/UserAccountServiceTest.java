package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса UserAccountService
 */
@SpringBootTest
class UserAccountServiceTest {

    /**
     * Внедрение зависимости UserAccountService
     */
    @Autowired
    private UserAccountService userAccountService;

    /**
     * Внедрение подменной зависимости UserAccountRepository
     */
    @MockBean
    private UserAccountDaoImpl userAccountRepository;

    /**
     * Поле UserAccount
     */
    private UserAccount userAccount;

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    public void setUp() {
        this.userAccount = new UserAccount(
                1L,
                "UserLogin1",
                "firstNameUserAccount1",
                "lastNameUserAccount1",
                "email@user1.com",
                "password1",
                "password1",
                LocalDate.parse("1991-01-01"),
                "ROLE_USER",
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );
    }

    /**
     * Должен сохранить аккаунт в базу данных
     */
    @Test
    public void shouldSaveUserAccount() {

        /* создаем подменный репозиторий и указываем что метод  save() должен вернуть сохраненный объект */
        when(userAccountRepository.save(userAccount)).thenReturn(userAccount);
        /* сохраняем объект userAccount1 */
        UserAccount savedUserAccount = userAccountService.save(userAccount);
        /* проверяем что сохранение проходит успешно */
        verify(userAccountRepository).save(userAccount);
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedUserAccount).hasFieldOrPropertyWithValue("userId", userAccount.getUserId());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedUserAccount).hasFieldOrPropertyWithValue("userLogin", userAccount.getUserLogin());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedUserAccount).hasFieldOrPropertyWithValue("firstName", userAccount.getFirstName());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedUserAccount).hasFieldOrPropertyWithValue("lastName", userAccount.getLastName());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedUserAccount).hasFieldOrPropertyWithValue("email", userAccount.getEmail());

    }

    /**
     * Должен обновить аккаунт по id
     */
    @Test
    public void shouldUpdateUserAccountById() {
        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(userAccountRepository.save(userAccount)).thenReturn(userAccount);
        /* сохраняем объект userAccount1 */
        UserAccount savedUserAccount = userAccountService.save(userAccount);
        /* создаем новый логин для юзера */
        String newUserLogin = "New User login";
        /* создаем новое имя для юзера */
        String newUserFirstName = "New User first name";
        /* создаем новую фамилию для юзера */
        String newUserLastName = "New User last name";
        /* создаем новую почту для юзера */
        String newUserEmail = "New User email";

        /* устанавливаем новое значение логина */
        savedUserAccount.setUserLogin(newUserLogin);
        /* устанавливаем новое значение имени */
        savedUserAccount.setFirstName(newUserFirstName);
        /* устанавливаем новое значение фамилии */
        savedUserAccount.setLastName(newUserLastName);
        /* устанавливаем новое значение почты */
        savedUserAccount.setEmail(newUserEmail);
        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(userAccountRepository.save(savedUserAccount)).thenReturn(savedUserAccount);
        /* сохраняем объект с обновленными полями */
        UserAccount updatedUserAccount = userAccountService.save(savedUserAccount);
        /* утверждаем что обновленный объект имеет такой же id как и первоначально сохраненный */
        assertThat(updatedUserAccount.getUserId()).isEqualTo(savedUserAccount.getUserId());
        /* утверждаем что обновленный объект имеет новый логин */
        assertThat(updatedUserAccount.getUserLogin()).isEqualTo(newUserLogin);

        /* утверждаем что обновленный объект имеет новый логин */
        assertThat(updatedUserAccount.getFirstName()).isEqualTo(newUserFirstName);
        /* утверждаем что обновленный объект имеет новый логин */
        assertThat(updatedUserAccount.getLastName()).isEqualTo(newUserLastName);
        /* утверждаем что обновленный объект имеет новый логин */
        assertThat(updatedUserAccount.getEmail()).isEqualTo(newUserEmail);
    }

    /**
     * Должен удалить аккаунт по id
     */
    @Test
    public void shouldDeleteUserAccountById() {
        /* удаляем объект по id*/
        userAccountService.deleteById(userAccount.getUserId());
        /* проверяем что удаление проходит успешно */
        verify(userAccountRepository).deleteById(userAccount.getUserId());
    }

    /**
     * Должен найти все аккаунты
     */
    @Test
    void shouldFindAllUserAccounts() {

        /* создаем подменный репозиторий и указываем что метод findAll() должен вернуть 3 объекта */
        when(userAccountRepository.findAll()).thenReturn(Arrays.asList(userAccount, userAccount, userAccount));
        /* получаем 3 объекта Team */
        List<UserAccount> userAccountList = userAccountService.findAll();
        /* утверждаем что размер списка равен 3 */
        Assertions.assertEquals(3, userAccountList.size(), "метод findAll() должен вернуть 3 объекта");
    }

    /**
     * Должен найти аккаунт по id
     */
    @Test
    void shouldFindUserAccountById() {

        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(userAccountRepository.findById(anyLong())).thenReturn(userAccount);
        /* получаем объект по id */
        Optional<UserAccount> returnedUserAccount = userAccountService.findById(1L);
        /* проверяем что поиск проходит успешно */
        verify(userAccountRepository).findById(1L);
        /* утверждаем что найденный объект есть */
        Assertions.assertTrue(returnedUserAccount.isPresent(), "Объект не был найден");
        /* утверждаем что найденный объект равен подменному объекту */
        Assertions.assertSame(returnedUserAccount.get(), userAccount, "Найденный объект не равен подменному объекту");
    }

}