package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.ProjectRepository;
import com.konstantinbulygin.pmwebapp.entities.Project;
import com.konstantinbulygin.pmwebapp.entities.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса ProjectService
 */
@SpringBootTest
class ProjectServiceTest {

    /**
     * Внедрение зависимости ProjectService
     */
    @Autowired
    private ProjectService projectService;

    /**
     * Внедрение подменной зависимости ProjectRepository
     */
    @MockBean
    private ProjectRepository projectRepository;

    /**
     * Поле Project
     */
    private Project project;

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    public void setUp() {
        List<Team> teamList = new ArrayList<>();
        LocalDate dateStart = LocalDate.parse("2018-01-01");
        LocalDate dateEnd = dateStart.plusWeeks(1);
        this.project = new Project(1L, "Project1", 1L, "Project1 description", dateStart, dateEnd, teamList);
    }

    /**
     * Должен сохранить проект в базу данных
     */
    @Test
    public void shouldSaveProject() {

        /* создаем подменный репозиторий и указываем что метод  findById(1) должен вернуть ранее созданную роль */
        when(projectRepository.save(project)).thenReturn(project);
        /* сохраняем объект project1 */
        Project savedProject = projectService.save(project);
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("projectId", project.getProjectId());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("projectName", project.getProjectName());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("statusId", project.getStatusId());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("description", project.getDescription());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("startDate", project.getStartDate());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("endDate", project.getEndDate());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProject).hasFieldOrPropertyWithValue("teams", project.getTeams());
    }

    /**
     * Должен обновить проект по id
     */
    @Test
    public void shouldUpdateProjectById() {

        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(projectRepository.save(project)).thenReturn(project);
        /* сохраняем объект project */
        Project savedProject = projectService.save(project);
        /* создаем новое название проекта */
        String newProjectName = "NewProjectName";
        /* создаем новое описание проекта */
        String newProjectDescription = "NewProjectDescription";
        /* устанавливаем новое значение для поля projectName */
        savedProject.setProjectName(newProjectName);
        /* устанавливаем новое значение для поля description */
        savedProject.setDescription(newProjectDescription);

        /* создаем подменный репозиторий и указываем что метод findById(id) должен вернуть объект project */
        when(projectRepository.save(savedProject)).thenReturn(savedProject);
        /* обновляем объект savedProject и получаем обновленный объект */
        Project updatedProject = projectService.save(savedProject);

        /* утверждаем что обновленный объект имеет такой же id как и первоначально сохраненный */
        assertThat(updatedProject.getProjectId()).isEqualTo(savedProject.getProjectId());
        /* утверждаем что обновленный объект имеет новое название */
        assertThat(updatedProject.getProjectName()).isEqualTo(newProjectName);
        /* утверждаем что обновленный объект имеет новое описание */
        assertThat(updatedProject.getDescription()).isEqualTo(newProjectDescription);
    }

    /**
     * Должен удалить проект по id
     */
    @Test
    void shouldDeleteProjectById() {
        /* удаляем объект по id */
        projectService.deleteById(1L);
        /* проверяем что удаление проходит успешно */
        verify(projectRepository).deleteById(1L);
    }

    /**
     * Должен найти все проекты
     */
    @Test
    void shouldFindAllProjects() {

        /* создаем подменный репозиторий и указываем что метод findAll() должен вернуть 3 объекта */
        when(projectRepository.findAll()).thenReturn(Arrays.asList(project, project, project));
        /* получаем 3 объекта */
        List<Project> projects = projectService.findAll();
        /* утверждаем что размер списка равен 3 */
        Assertions.assertEquals(3, projects.size(), "findAll() должен вернуть 3 объекта");
    }

    /**
     * Должен найти проект по id
     */
    @Test
    void shouldFindProjectById() {
        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(projectRepository.findById(1L)).thenReturn(Optional.of(project));
        /* получаем объект по id */
        Optional<Project> returnedProject = projectService.findById(1);
        /* утверждаем что найденный объект есть */
        Assertions.assertTrue(returnedProject.isPresent(), "Проект не был найден");
        /* утверждаем что найденный объект равен подменному объекту */
        Assertions.assertSame(returnedProject.get(), project, "Найденный проект не равен подменному проекту");
    }
}