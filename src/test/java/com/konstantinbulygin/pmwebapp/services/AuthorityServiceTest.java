package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.AuthorityRepository;
import com.konstantinbulygin.pmwebapp.entities.Authority;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Тестирование сервиса AuthorityService
 */
@SpringBootTest
class AuthorityServiceTest {

    /**
     * Внедрение зависимости AuthorityService
     */
    @Autowired
    private AuthorityService authorityService;

    /**
     * Внедрение подменной зависимости AuthorityRepository
     */
    @MockBean
    private AuthorityRepository authorityRepository;

    /**
     * Поле Authority
     */
    private Authority authority;

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    void setUp() {
        /* создаем роль */
        authority = new Authority(1, "ROLE_ADMIN");
    }

    /**
     * Должен найти роль по id
     */
    @Test
    void shouldFindAuthorityById() {

        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(authorityRepository.findById(anyInt())).thenReturn(Optional.of(authority));
        /* ищем роль по id */
        Optional<Authority> returnedAuthority = authorityService.findById(1);
        /* утверждаем что возвращенная роль существует */
        Assertions.assertTrue(returnedAuthority.isPresent(), "Роль не найдена");
        /* утверждаем что возвращенная роль равна созданной роли */
        Assertions.assertSame(returnedAuthority.get(), authority, "Роль найденная не совпадает с имитационной ролью");
    }

    /**
     * Должен вернуть пустой список
     */
    @Test
    public void shouldFindNoAuthorityIfRepositoryIsEmpty() {
        /* получаем список всех ролей */
        Iterable<Authority> authorities = authorityService.findAll();
        /* утверждаем что список пуст */
        assertThat(authorities).isEmpty();
    }

    /**
     * Не должен найти роль по Id
     */
    @Test
    void shouldNotFoundAuthorityById() {
        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(authorityRepository.findById(anyInt())).thenReturn(Optional.empty());
        /* ищем роль */
        Optional<Authority> returnedAuthority = authorityService.findById(1);
        /* утверждаем что роли нет */
        Assertions.assertFalse(returnedAuthority.isPresent(), "Роль не должна быть найдена");
    }


    /**
     * Должен найти все роли
     */
    @Test
    void shouldFindAllAuthorities() {

        /* создаем подменный репозиторий и указываем что метод  findAll() должен вернуть 2 объекта */
        when(authorityRepository.findAll()).thenReturn(Arrays.asList(authority, authority));
        /* получаем все роли */
        List<Authority> authorities = authorityService.findAll();
        /* утверждаем что размер списка должен быть равен 2 */
        Assertions.assertEquals(2, authorities.size(), "findAll() должен вернуть 2 роли");
    }
}