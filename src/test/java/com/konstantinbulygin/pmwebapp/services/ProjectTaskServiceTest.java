package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.ProjectTaskRepository;
import com.konstantinbulygin.pmwebapp.entities.ProjectTask;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Тестирование сервиса ProjectTaskService
 */
@SpringBootTest
class ProjectTaskServiceTest {

    /**
     * Внедрение зависимости ProjectTaskService
     */
    @Autowired
    private ProjectTaskService projectTaskService;

    /**
     * Внедрение подменной зависимости ProjectTaskRepository
     */
    @MockBean
    private ProjectTaskRepository projectTaskRepository;

    /**
     * Поле ProjectTask
     */
    private ProjectTask projectTask;

    ProjectTaskServiceTest() {
    }

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    public void setUp() {
        this.projectTask = new ProjectTask(1L, 1L, "ProjectTask", 1L, 1L);
    }

    /**
     * Должен сохранить задачу в базу данных
     */
    @Test
    public void shouldSaveProjectTask() {

        /* создаем подменный репозиторий и указываем что метод  save() должен вернуть сохраненный объект */
        when(projectTaskRepository.save(projectTask)).thenReturn(projectTask);
        /* сохраняем объект projectTask */
        ProjectTask savedProjectTask = projectTaskService.save(projectTask);
        /* проверяем что сохранение проходит успешно */
        verify(projectTaskRepository).save(projectTask);
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProjectTask).hasFieldOrPropertyWithValue("taskId", projectTask.getTaskId());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProjectTask).hasFieldOrPropertyWithValue("projectId", projectTask.getProjectId());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProjectTask).hasFieldOrPropertyWithValue("description", projectTask.getDescription());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProjectTask).hasFieldOrPropertyWithValue("teamId", projectTask.getTeamId());
        /* утверждаем что сохраненный объект имеет указанные поля и их значения */
        assertThat(savedProjectTask).hasFieldOrPropertyWithValue("statusId", projectTask.getStatusId());
    }

    /**
     * Должен обновить задачу по id
     */
    @Test
    public void shouldUpdateProjectTaskById() {

        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(projectTaskRepository.save(projectTask)).thenReturn(projectTask);
        /* сохраняем объект projectTask */
        ProjectTask savedProjectTask = projectTaskService.save(projectTask);
        /* создаем новое описание проекта */
        String newProjectTaskDescription = "New ProjectTask description";
        /* устанавливаем новое значение */
        savedProjectTask.setDescription(newProjectTaskDescription);

        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(projectTaskRepository.save(savedProjectTask)).thenReturn(savedProjectTask);
        /* сохраняем объект с обновленным полем */
        ProjectTask updatedProjectTask = projectTaskService.save(savedProjectTask);

        /* утверждаем что обновленный объект имеет такой же id как и первоначально сохраненный */
        assertThat(updatedProjectTask.getTaskId()).isEqualTo(savedProjectTask.getTaskId());
        /* утверждаем что обновленный объект имеет такой же id проекта как и первоначально сохраненный */
        assertThat(updatedProjectTask.getProjectId()).isEqualTo(savedProjectTask.getProjectId());
        /* утверждаем что обновленный объект имеет новое описание */
        assertThat(updatedProjectTask.getDescription()).isEqualTo(newProjectTaskDescription);
    }

    /**
     * Должен удалить задачу по id
     */
    @Test
    public void shouldDeleteProjectTaskById() {
        /* удаляем */
        projectTaskService.delete(projectTask.getTaskId());
        /* проверяем что удаление проходит успешно */
        verify(projectTaskRepository).deleteById(projectTask.getTaskId());
    }

    /**
     * Должен найти все задачи
     */
    @Test
    void shouldFindAllProjectTasks() {

        /* создаем подменный репозиторий и указываем что метод findAll() должен вернуть 3 объекта */
        when(projectTaskRepository.findAll()).thenReturn(Arrays.asList(projectTask, projectTask, projectTask));
        /* получаем 3 объекта ProjectTask */
        List<ProjectTask> projectTasks = projectTaskService.findAll();
        /* проверяем что поиск проходит успешно */
        verify(projectTaskRepository).findAll();
        /* утверждаем что размер списка равен 3 */
        Assertions.assertEquals(3, projectTasks.size(), "findAll() должен вернуть 3 объекта");
    }

    /**
     * Должен найти задачу по id
     */
    @Test
    void shouldFindProjectTaskById() {
        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(projectTaskRepository.findById(anyLong())).thenReturn(Optional.of(projectTask));
        /* получаем объект по id */
        Optional<ProjectTask> returnedProjectTask = projectTaskService.findById(projectTask.getTaskId());
        /* проверяем что поиск проходит успешно */
        verify(projectTaskRepository).findById(projectTask.getTaskId());
        /* утверждаем что найденный объект есть */
        Assertions.assertTrue(returnedProjectTask.isPresent(), "Объект не был найден");
        /* утверждаем что найденный объект равен подменному объекту */
        Assertions.assertSame(returnedProjectTask.get(), projectTask, "Найденный объект не равен подменному объекту");
    }

}