package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.TaskStatusRepository;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Тестирование сервиса TaskStatusService
 */
@SpringBootTest
class TaskStatusServiceTest {

    /**
     * Внедрение зависимости TaskStatusService
     */
    @Autowired
    TaskStatusService taskStatusService;

    /**
     * Внедрение подменной зависимости TaskStatusRepository
     */
    @MockBean
    TaskStatusRepository taskStatusRepository;

    /**
     * Поле Team
     */
    private TaskStatus taskStatus;

    /**
     * Вызывается перед каждым тестом
     */
    @BeforeEach
    public void setUp() {
        this.taskStatus = new TaskStatus(1L, "TaskStatus");
    }

    /**
     * Должен сохранить статус в базу данных
     */
    @Test
    public void shouldSaveTaskStatus() {

        /* создаем подменный репозиторий и указываем что метод  save() должен вернуть сохраненный объект */
        when(taskStatusRepository.save(taskStatus)).thenReturn(taskStatus);
        /* сохраняем объект taskStatus */
        TaskStatus savedTaskTask = taskStatusService.save(taskStatus);
        /* проверяем что сохранение проходит успешно */
        verify(taskStatusRepository).save(taskStatus);
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedTaskTask).hasFieldOrPropertyWithValue("statusId", taskStatus.getStatusId());
        /* утверждаем что сохраненный объект имеет указанное поле и его значение */
        assertThat(savedTaskTask).hasFieldOrPropertyWithValue("statusName", taskStatus.getStatusName());
    }

    /**
     * Должен обновить команду по id
     */
    @Test
    public void shouldUpdateTaskStatusById() {

        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(taskStatusRepository.save(taskStatus)).thenReturn(taskStatus);
        /* сохраняем объект taskStatus */
        TaskStatus savedTaskStatus = taskStatusService.save(taskStatus);
        /* создаем новое название статуса */
        String newStatusName = "New TaskStatus name";
        /* устанавливаем новое значение */
        savedTaskStatus.setStatusName(newStatusName);

        /* создаем подменный репозиторий и указываем что метод save должен вернуть сохраненный объект */
        when(taskStatusRepository.save(savedTaskStatus)).thenReturn(savedTaskStatus);
        /* сохраняем объект с обновленным полем */
        TaskStatus updatedTaskStatus = taskStatusService.save(savedTaskStatus);
        /* утверждаем что обновленный объект имеет такой же id как и первоначально сохраненный */
        assertThat(updatedTaskStatus.getStatusId()).isEqualTo(savedTaskStatus.getStatusId());
        /* утверждаем что обновленный объект имеет новое описание */
        assertThat(updatedTaskStatus.getStatusName()).isEqualTo(newStatusName);
    }

    /**
     * Должен удалить статус по id
     */
    @Test
    public void shouldDeleteTaskStatusById() {
        /* удаляем объект по id*/
        taskStatusService.delete(taskStatus.getStatusId());
        /* проверяем что удаление проходит успешно */
        verify(taskStatusRepository).deleteById(taskStatus.getStatusId());
    }

    /**
     * Должен найти все статусы
     */
    @Test
    void shouldFindAllTaskStatus() {

        /* создаем подменный репозиторий и указываем что метод findAll() должен вернуть 3 объекта */
        when(taskStatusRepository.findAll()).thenReturn(Arrays.asList(taskStatus, taskStatus, taskStatus));
        /* получаем 3 объекта TaskStatus */
        List<TaskStatus> taskStatuses = taskStatusService.findAll();
        /* утверждаем что размер списка равен 3 */
        Assertions.assertEquals(3, taskStatuses.size(), "метод findAll() должен вернуть 3 объекта");
    }

    /**
     * Должен найти статус по id
     */
    @Test
    void shouldFindTaskStatusByIid() {

        /* создаем подменный репозиторий и указываем что метод findById(1L) должен вернуть 1 объект */
        when(taskStatusRepository.findById(anyLong())).thenReturn(Optional.of(taskStatus));
        /* получаем объект по id */
        Optional<TaskStatus> returnedTaskStatus = taskStatusService.findById(taskStatus.getStatusId());
        /* проверяем что поиск проходит успешно */
        verify(taskStatusRepository).findById(taskStatus.getStatusId());
        /* утверждаем что найденный объект есть */
        Assertions.assertTrue(returnedTaskStatus.isPresent(), "Объект не был найден");
        /* утверждаем что найденный объект равен подменному объекту */
        Assertions.assertSame(returnedTaskStatus.get(), taskStatus, "Найденный объект не равен подменному объекту");
    }

}