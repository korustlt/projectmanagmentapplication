package com.konstantinbulygin.pmwebapp.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
class ExceptionControllerTest {

    @InjectMocks
    ExceptionController exceptionController;
    @Mock
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(exceptionController).build();
    }

    @Test
    void shouldReturnViewWithError() throws Exception {

        this.mvc
                .perform(get("/error"))
                .andExpect(status().isOk())
                .andExpect(view().name("security/error"));
    }
}