package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.AuthorityService;
import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class SecurityControllerTest {

    private static MockHttpServletRequest request;
    @Mock
    private MockMvc mvc;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private AuthorityService authorityService;

    @InjectMocks
    SecurityController securityController;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(securityController).build();
    }

    @BeforeAll
    public static void setUpBeforeAll() {
        request = new MockHttpServletRequest();
        request.setParameter("userLogin", "UserLogin1");
        request.setParameter("firstName", "UserFirstName1");
        request.setParameter("lastName", "UserLastNAme1");
        request.setParameter("email", "user1@gmail.com");
        request.setParameter("password", "password1");
        request.setParameter("confirmPassword", "password1");
        request.setParameter("userDob", "1991-01-01");
        request.setParameter("authorities", "ROLE_ADMIN");
        request.setParameter("statusName", "Готов");
        request.setParameter("teamName", "teamName");
        request.setAttribute("teamUsers", List.of(new UserAccount()));
    }

    @Test
    void shouldReturnViewWithRegisterForm() throws Exception {

        this.mvc
                .perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(view().name("security/register"))
                .andExpect(model().attributeExists("userAccount"))
                .andExpect(model().attributeExists("allAuthorities"));
    }

    @Test
    void shouldRegisterNewUserAndReturnLoginView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(post("/register/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userLogin", request.getParameterValues("userLogin"))
                        .param("firstName", request.getParameterValues("firstName"))
                        .param("lastName", request.getParameterValues("lastName"))
                        .param("email", request.getParameterValues("email"))
                        .param("password", request.getParameterValues("password"))
                        .param("confirmPassword", request.getParameterValues("confirmPassword"))
                        .param("userDob", request.getParameterValues("userDob"))
                        .param("authorities", request.getParameterValues("authorities"))
                )
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/login");
    }


    @Test
    void shouldDisplayLoginForm() throws Exception {

        this.mvc
                .perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("security/login"));
    }
}