package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.entities.Project;
import com.konstantinbulygin.pmwebapp.entities.ProjectTask;
import com.konstantinbulygin.pmwebapp.entities.Team;
import com.konstantinbulygin.pmwebapp.services.*;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ProjectControllerTest {

    private static MockHttpServletRequest request;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private EntityManager entityManager;

//    @Mock
//    private MockMvc mvc;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private TaskStatusService taskStatusService;

    @Mock
    private TeamService teamService;

    @Mock
    private ProjectService projectService;

    @Mock
    private ProjectTaskService projectTaskService;

    @InjectMocks
    ProjectController projectController;

    Page<Project> projectPage;
    Page<ProjectTask> taskPage;

    @BeforeEach
    void setUp() {
        projectController.setEntityManager(entityManager);
        taskPage = new PageImpl<>(List.of(new ProjectTask(), new ProjectTask(), new ProjectTask()), PageRequest.of(0, 3), 3);
        projectPage = new PageImpl<>(List.of(new Project(), new Project(), new Project()), PageRequest.of(0, 3), 3);
        mvc = MockMvcBuilders.standaloneSetup(projectController).build();
    }

    @BeforeAll
    public static void setUpBeforeAll() {
        request = new MockHttpServletRequest();
        request.setParameter("projectName", "projectName1");
        request.setParameter("statusId", "1");
        request.setParameter("description", "description1");
        request.setParameter("startDate", "2023-01-01");
        request.setParameter("endDate", "2023-01-10");
        request.setParameter("users", "users");
        request.setParameter("taskProjectId", "1");
        request.setParameter("taskDescription", "taskDescription");
        request.setParameter("taskTeamId", "1");
        request.setParameter("taskStatusId", "1");
    }

    @Test
    void shouldReturnViewWithPrefilledData() throws Exception {


        when(projectService.findProjectsPaginated(any())).thenReturn(projectPage);

        this.mvc
                .perform(get("/projects"))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/list-projects"))
                .andExpect(model().attribute("pageNumbers", hasSize(projectPage.getTotalPages())))
                .andExpect(model().attributeExists("projectPage"))
                .andExpect(model().attribute("projectPage", hasProperty("totalElements", equalTo(3L))));
    }

    @Test
    void shouldReturnViewToCreateNewProject() throws Exception {

        this.mvc.perform(get("/projects/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/new-project"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attributeExists("project"));
    }

    @Test
    void shouldCreateNewProjectAndReturnView() throws Exception {

        Project project = new Project();
        project.setProjectName("projectName");
        project.setStatusId(1L);
        project.setDescription("description");
        project.setStartDate(LocalDate.EPOCH);
        project.setEndDate(LocalDate.EPOCH);
        project.setTeams(List.of(new Team()));


        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders
                        .post("/projects/save")
                        .flashAttr("project", project))
                .andExpect(status().is(302))
                .andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }


//    @Test
//    void shouldCatchExceptionWhenCreateNewProjectWithNullUsers() throws Exception {
//
//        Project project = new Project();
//        project.setProjectName("projectName");
//        project.setStatusId(1L);
//        project.setDescription("description");
//        project.setStartDate(LocalDate.EPOCH);
//        project.setEndDate(LocalDate.EPOCH);
//        project.setUsers(null);
//
//
//        this.mvc.perform(MockMvcRequestBuilders
//                        .post("/projects/save")
//                        .flashAttr("project", project))
//                .andExpect(result -> Assertions.assertNotNull(result.getResolvedException()));
//
//    }

    @Test
    void shouldReturnViewToEditProject() throws Exception {

        when(projectService.findById(anyLong())).thenReturn(Optional.of(new Project()));

        this.mvc.perform(get("/projects/edit/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/edit-project"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("project"));
    }

    @Test
    void shouldSaveEditedProjectAndReturnView() throws Exception {

        Project project = new Project();
        project.setProjectName("projectName");
        project.setStatusId(1L);
        project.setDescription("description");
        project.setStartDate(LocalDate.EPOCH);
        project.setEndDate(LocalDate.EPOCH);
        project.setTeams(List.of(new Team()));


        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders
                        .post("/projects/save")
                        .flashAttr("project", project))
                .andExpect(status().is(302))
                .andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }

    @Test
    void shouldDeleteProjectAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(get("/projects/delete/{id}", 1))
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }

    //=================================================
    @Test
    void shouldReturnViewWithTasksPrefilledData() throws Exception {

        when(projectTaskService.findProjectTasksPaginated(any(), anyLong())).thenReturn(taskPage);
        when(projectService.findById(anyLong())).thenReturn(Optional.of(new Project()));

        this.mvc
                .perform(get("/projects/show/tasks/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/list-tasks"))
                .andExpect(model().attribute("pageNumbers", hasSize(taskPage.getTotalPages())))
                .andExpect(model().attributeExists("projectTasksPage"))
                .andExpect(model().attributeExists("project"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attribute("projectTasksPage", hasProperty("totalElements", equalTo(3L))));
    }

    @Test
    void shouldReturnViewToCreateNewTaskProject() throws Exception {

        this.mvc.perform(get("/projects/add/task/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/new-task"))
                .andExpect(model().attributeExists("allProjects"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("task"));
    }

    @Test
    void shouldSaveNewTaskProjectAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(post("/projects/task/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("projectId", request.getParameterValues("taskProjectId"))
                        .param("description", request.getParameterValues("taskDescription"))
                        .param("teamId", request.getParameterValues("taskTeamId"))
                        .param("statusId", request.getParameterValues("taskStatusId"))
                )
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }

    @Test
    void shouldReturnViewToEditTaskProject() throws Exception {

        when(projectTaskService.findById(anyLong())).thenReturn(Optional.of(new ProjectTask()));

        this.mvc.perform(get("/projects/edit/task/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/edit-task"))
                .andExpect(model().attributeExists("task"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("allProjects"));
    }

    @Test
    void shouldSaveEditedTaskProjectAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(post("/projects/task/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("projectId", request.getParameterValues("taskProjectId"))
                        .param("description", request.getParameterValues("taskDescription"))
                        .param("teamId", request.getParameterValues("taskTeamId"))
                        .param("statusId", request.getParameterValues("taskStatusId"))
                )
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }

    @Test
    void shouldDeleteTaskProjectAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(get("/projects/delete/task/{id}", 1))
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/projects");
    }


    @Test
    void shouldReturnViewWithProjectTimeLines() throws Exception {

        this.mvc
                .perform(get("/projects/timelines"))
                .andExpect(status().isOk())
                .andExpect(view().name("projects/project-timelines"))
                .andExpect(model().attributeExists("projectListIsEmpty"));
    }

}