package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.services.ProjectService;
import com.konstantinbulygin.pmwebapp.services.TaskStatusService;
import com.konstantinbulygin.pmwebapp.services.TeamService;
import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class HomeControllerTest {

    @Mock
    private MockMvc mvc;

    @InjectMocks
    HomeController homeController;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private TaskStatusService taskStatusService;

    @Mock
    private TeamService teamService;

    @Mock
    private ProjectService projectService;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(homeController).build();
    }

    @Test
    void shouldReturnViewWithPrefilledData() throws Exception {

        this.mvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("main/projects-info"))
                .andExpect(model().attributeExists("projectsList"))
                .andExpect(model().attributeExists("teamProject"))
                .andExpect(model().attributeExists("allTeams"))
                .andExpect(model().attributeExists("allStatuses"))
                .andExpect(model().attributeExists("allUsers"))
                .andExpect(model().attributeExists("projectStatusCnt"));
    }

    @Test
    void shouldReturnViewWithAllStatusesUsingAjax() throws Exception {
        this.mvc
                .perform(get("/getAllStatuses"))
                .andExpect(status().isOk())
                .andExpect(view().name("fragments/ajaxPart :: all-statuses"));
    }
}