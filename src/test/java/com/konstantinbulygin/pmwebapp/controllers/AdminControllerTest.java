package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.dao.AuthorityRepository;
import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import com.konstantinbulygin.pmwebapp.entities.Team;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.*;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class AdminControllerTest {

    private static MockHttpServletRequest request;
    @Mock
    private ProjectService projectService;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private AuthorityService authorityService;

    @Mock
    AuthorityRepository authorityRepository;

    @Mock
    private TaskStatusService taskStatusService;

    @Mock
    private TeamService teamService;

    @Mock
    private MockMvc mvc;

    @Mock
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    AdminController adminController;

    Page<UserAccount> userPage;
    Page<TaskStatus> statusPage;

    Page<Team> teamPage;

    @BeforeAll
    public static void setUpBeforeAll() {
        request = new MockHttpServletRequest();
        request.setParameter("userLogin", "UserLogin1");
        request.setParameter("firstName", "UserFirstName1");
        request.setParameter("lastName", "UserLastNAme1");
        request.setParameter("email", "user1@gmail.com");
        request.setParameter("password", "password1");
        request.setParameter("confirmPassword", "password1");
        request.setParameter("userDob", "1991-01-01");
        request.setParameter("statusName", "Готов");
        request.setParameter("teamName", "teamName");
    }

    @BeforeEach
    void setUp() {
        statusPage = new PageImpl<>(List.of(new TaskStatus(), new TaskStatus(), new TaskStatus()), PageRequest.of(0, 3), 3);
        userPage = new PageImpl<>(List.of(new UserAccount(), new UserAccount(), new UserAccount()), PageRequest.of(0, 3), 3);
        teamPage = new PageImpl<>(List.of(new Team(), new Team(), new Team()), PageRequest.of(0, 3), 3);
        mvc = MockMvcBuilders.standaloneSetup(adminController).build();
    }

    @Test
    void shouldReturnViewWithPrefilledDataOfUsers() throws Exception {
        when(userAccountService.findUsersPaginated(any())).thenReturn(userPage);
        this.mvc
                .perform(get("/admin/users"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/list-users"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attributeExists("userPage"))
                .andExpect(model().attribute("userPage", hasProperty("totalElements")));
    }

    @Test
    void shouldReturnViewToCreateNewUser() throws Exception {

        this.mvc.perform(get("/admin/user/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("users/new-user"))
                .andExpect(model().attributeExists("userAccount"))
                .andExpect(model().attributeExists("allAuthorities"));
    }

    @Test
    void shouldCreateNewUser() throws Exception {

        UserAccount user = new UserAccount();
        user.setEmail("email@gmail.com");
        user.setPassword("123456");
        user.setAuthorities(List.of(new Authority()));
        user.setLastName("lastName");
        user.setUserLogin("userLogin");
        user.setFirstName("firstName");
        user.setUserDob(LocalDate.EPOCH);

        this.mvc.perform(MockMvcRequestBuilders
                        .post("/admin/user/save")
                        .flashAttr("userAccount", user))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void shouldSaveEditedUserAndReturnView() throws Exception {

        UserAccount user = new UserAccount();
        user.setEmail("email@gmail.com");
        user.setPassword("123456");
        user.setAuthorities(List.of(new Authority()));
        user.setLastName("lastName");
        user.setUserLogin("userLogin");
        user.setFirstName("firstName");
        user.setUserDob(LocalDate.EPOCH);

        this.mvc.perform(MockMvcRequestBuilders
                        .post("/admin/user/save")
                        .flashAttr("userAccount", user))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void shouldDeleteUserAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(get("/admin/delete/{id}", 1))
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/users");
    }

    @Test
    void shouldReturnViewWithPrefilledDataOfStatuses() throws Exception {
        when(taskStatusService.findStatusesPaginated(any())).thenReturn(statusPage);
        this.mvc
                .perform(get("/admin/statuses"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/list-statuses"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attributeExists("statusPage"))
                .andExpect(model().attribute("statusPage", hasProperty("totalElements")));
    }

    @Test
    void shouldReturnViewToCreateNewStatus() throws Exception {

        this.mvc.perform(get("/admin/status/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("statuses/new-status"))
                .andExpect(model().attributeExists("statusNew"));
    }

    @Test
    void shouldCreateNewStatusAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(post("/admin/status/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("statusName", request.getParameterValues("statusName"))
                )
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/statuses");
    }

    @Test
    void shouldReturnViewToEditStatus() throws Exception {

        this.mvc.perform(get("/admin/status/edit/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/edit-status"));
    }

    @Test
    void shouldSaveEditedStatusAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(post("/admin/status/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("statusName", request.getParameterValues("statusName"))
                )
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/statuses");
    }

    @Test
    void shouldReturnViewWithPrefilledDataOfTeams() throws Exception {
        when(teamService.findTeamsPaginated(any())).thenReturn(teamPage);
        this.mvc
                .perform(get("/admin/teams"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/list-teams"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attributeExists("teamPage"))
                .andExpect(model().attribute("teamPage", hasProperty("totalElements")));
    }

    @Test
    void shouldReturnViewToCreateNewTeam() throws Exception {

        this.mvc.perform(get("/admin/team/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("teams/new-team"))
                .andExpect(model().attributeExists("team"))
                .andExpect(model().attributeExists("allUsers"));
    }

    @Test
    void shouldSaveNewTeamAndReturnView() throws Exception {

        Team team = new Team();
        team.setTeamName("Some Team");
        team.setTeamUsers(List.of(new UserAccount()));

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders
                        .post("/admin/team/save")
                        .flashAttr("team", team))
                .andExpect(MockMvcResultMatchers.status().is(302))
                .andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/teams");
    }

    @Test
    void shouldReturnViewToEditTeam() throws Exception {

        this.mvc.perform(get("/admin/team/edit/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/edit-team"))
                .andExpect(model().attributeExists("allUsers"));
    }

    @Test
    void shouldSaveEditedTeamAndReturnView() throws Exception {

        Team team = new Team();
        team.setTeamName("New Team");
        team.setTeamUsers(List.of(new UserAccount()));

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders
                        .post("/admin/team/save")
                        .flashAttr("team", team))
                .andExpect(MockMvcResultMatchers.status().is(302))
                .andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/teams");
    }

    @Test
    void shouldDeleteTeamAndReturnView() throws Exception {

        MvcResult mvcResult = this.mvc.perform(get("/admin/team/delete/{id}", 1))
                .andExpect(status().is(302)).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();
        assert mav != null;
        ModelAndViewAssert.assertViewName(mav, "redirect:/admin/teams");
    }
}