let niceChartData = decodeHtml(chartData);
let chartJsonArray = JSON.parse(niceChartData);

let numericData = [];
let labelData = [];

for (let i = 0; i < chartJsonArray.length; i++) {
    labelData[i] = chartJsonArray[i].label;
    numericData[i] = chartJsonArray[i].value;
}

console.log(numericData)
console.log(labelData)

new Chart(document.getElementById("myPieChart"), {
    type: 'pie',

    data: {
        labels: labelData,
        datasets: [{
            label: 'My First dataset',
            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#9cba9f", "#3cbd9f", "#3abc9f", "#3def9f", "#3ghi9f", "#3jkl9f", "#3mno9f"],
            data: numericData
        }]
    },

    options: {
        title: {
            display: true,
            text: 'Статусы проектов'
        }
    }
});

function decodeHtml(html) {
    let txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
