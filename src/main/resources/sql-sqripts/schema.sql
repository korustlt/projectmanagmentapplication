CREATE TABLE IF NOT EXISTS `authorities` (
  `authority_id` int NOT NULL AUTO_INCREMENT,
  `authority_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `end_date` date NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `start_date` date NOT NULL,
  `status_id` bigint NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `fk_project_status_id_idx` (`status_id`),
  CONSTRAINT `fk_project_status_id` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`)
  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `project_id` bigint NOT NULL,
  `status_id` bigint NOT NULL,
  `team_id` bigint NOT NULL,
  PRIMARY KEY (`task_id`),
  KEY `fk_task_status_id_idx` (`status_id`),
  KEY `fk_task_project_id_idx` (`project_id`),
  KEY `fk_task_team_id_idx` (`team_id`),
  CONSTRAINT `fk_task_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_task_status_id` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_task_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `teams` (
  `team_id` bigint NOT NULL AUTO_INCREMENT,
  `team_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `user_accounts` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `dob` date NOT NULL,
  `user_login` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `project_team` (
  `project_id` bigint NOT NULL,
  `team_id` bigint NOT NULL,
  KEY `fk_project_team_id` (`team_id`),
  KEY `fk_team_project_id` (`project_id`),
  CONSTRAINT `fk_project_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_team_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `team_user` (
  `user_id` bigint NOT NULL,
  `team_id` bigint NOT NULL,
  KEY `fk_team_user_id` (`user_id`),
  KEY `fk_team_team_id` (`team_id`),
  CONSTRAINT `fk_team_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_team_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_accounts` (`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `users_authorities` (
  `user_id` bigint NOT NULL,
  `authority_id` int NOT NULL,
  KEY `fk_user_authority_id` (`authority_id`),
  KEY `fk_user_user_id` (`user_id`),
  CONSTRAINT `fk_user_authority_id` FOREIGN KEY (`authority_id`) REFERENCES `authorities` (`authority_id`),
  CONSTRAINT `fk_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_accounts` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;