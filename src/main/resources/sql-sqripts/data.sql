--INSERT ROLE
INSERT INTO `pma`.`authorities` (`authority_id`, `authority_name`) VALUES (1, 'ROLE_ADMIN');
INSERT INTO `pma`.`authorities` (`authority_id`, `authority_name`) VALUES (2, 'ROLE_USER');

--INSERT STATUSES
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (1, 'ГОТОВО');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (2, 'НЕГОТОВО');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (3, 'ТЕСТИРОВАНИЕ');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (4, 'ОБСУЖДЕНИЕ');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (5, 'НАЧАТО');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (6, 'НЕНАЧАТО');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (7, 'ПРИОСТАНОВЛЕННО');
INSERT INTO `pma`.`statuses` (`status_id`, `status_name`) VALUES (8, 'ДОРАБОТКА');

-- INSERT USERS
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (1, 'loginKonstantin', 'Konstantin', 'Bulygin', 'konstantin@yandex.ru', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-08-27', 'ROLE_ADMIN');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (2, 'loginJohn', 'John', 'Warton', 'warton@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-01-01', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (3, 'loginMike', 'Mike', 'Lanister', 'lanister@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-02-05', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (4, 'loginSteve', 'Steve', 'Reeves', 'Reeves@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-03-10', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (5, 'loginRonald','Ronald', 'Connor', 'connor@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-04-15', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (6, 'loginJim','Jim', 'Salvator', 'Sal@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-05-21', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (7, 'loginPeter','Peter', 'Henley', 'henley@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-06-23', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (8, 'loginRichard','Richard', 'Carson', 'carson@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-07-18', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (9, 'loginHonor','Honor', 'Miles', 'miles@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-08-06', 'ROLE_USER');
INSERT INTO `pma`.`user_accounts` (`user_id`, `user_login`, `first_name`, `last_name`, `email`, `password`, `dob`, `role`) values (10, 'loginTony','Tony', 'Roggers', 'roggers@gmail.com', '$2a$10$BJC4e8gQkVssL091IZoeP.jmCiKRFmcYV.jO8Rd41NEAu0mw7QTUe', '2023-09-30', 'ROLE_USER');