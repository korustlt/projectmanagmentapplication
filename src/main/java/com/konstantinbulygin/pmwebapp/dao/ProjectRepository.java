package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.dto.ChartData;
import com.konstantinbulygin.pmwebapp.dto.TimeChartData;
import com.konstantinbulygin.pmwebapp.entities.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий проектов
 */
@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    /**
     * Переопределенный метод findAll() из CrudRepository<T,T>
     * Возвращает список проектов из базы данных
     */
    @Override
    @NonNull
    List<Project> findAll();

    /**
     * SQL запрос в базу данных.
     * Возвращает список проектов и дат их выполнения
     */
    @Query(nativeQuery = true, value = "SELECT project_name AS projectName, start_date AS startDate, end_date AS endDate FROM projects")
    List<TimeChartData> getTimeData();

    /**
     * SQL запрос в базу данных.
     * Возвращает список проектов и их статусы
     */
    @Query(nativeQuery = true,
            value = "SELECT st.status_name AS Label, COUNT(*) AS Value " +
                    "FROM projects pr " +
                    "LEFT JOIN statuses st " +
                    "ON pr.status_id = st.status_id " +
                    "GROUP BY pr.status_id")
    List<ChartData> getProjectChartData();
}
