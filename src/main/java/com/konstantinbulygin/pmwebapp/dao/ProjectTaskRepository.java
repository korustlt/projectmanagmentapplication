package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.entities.ProjectTask;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий задач для проекта
 */
@Repository
public interface ProjectTaskRepository extends CrudRepository<ProjectTask, Long> {

    /**
     * Переопределенный метод findAll() из CrudRepository<T,T>
     * Возвращает список всех задач для проекта
     */
    @Override
    @NonNull
    List<ProjectTask> findAll();

    /**
     * Производит поиск задачи по id проекта
     * Возвращает задачу для проекта
     */
    List<ProjectTask> findByProjectId(long id);

}
