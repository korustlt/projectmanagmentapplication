package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий статусов
 */
@Repository
public interface TaskStatusRepository extends CrudRepository<TaskStatus, Long> {

    /**
     * Переопределенный метод findAll() из CrudRepository<T,T>
     * Возвращает список всех статусов
     */
    @NonNull
    @Override
    List<TaskStatus> findAll();
}
