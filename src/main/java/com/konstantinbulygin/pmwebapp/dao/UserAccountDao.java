package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.dto.UserTeamDto;
import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Репозиторий пользователей
 */
@Repository
public interface UserAccountDao {

    /**
     * Возвращает список пользователей
     */
    List<UserAccount> findAll();

    /**
     * Возвращает роль пользователя
     */
    Authority findAuthorities(String name);

    /**
     * Возвращает пользователей и команды в которых они участвуют
     */
    List<UserTeamDto> findTeamUsers();

    /**
     * Производит поиск по id и возвращает пользователя
     */
    UserAccount findById(long id);

    /**
     * Производит поиск по логину и возвращает пользователя
     */
    Optional<UserAccount> findByUserLogin(String login);

    /**
     * Сохраняет пользователя в базе данных и возвращает пользователя
     */
    UserAccount save(UserAccount userAccount);

    /**
     * Обновляет пользователя в базе данных и возвращает пользователя
     */
    UserAccount updateUser(UserAccount userAccount);
}
