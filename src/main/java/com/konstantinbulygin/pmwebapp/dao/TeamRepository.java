package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.dto.TeamProject;
import com.konstantinbulygin.pmwebapp.entities.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий команд
 */
@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {

    /**
     * Переопределенный метод findAll() из CrudRepository<T,T>
     * Возвращает список всех команд
     */
    @NonNull
    @Override
    List<Team> findAll();

    Team findByTeamId(long id);

    /**
     * Производит поиск пользователей и проектов
     */
    @Query(nativeQuery = true, value =
            "SELECT t.team_id AS teamId, t.team_name AS teamName,  COUNT(pt.team_id) AS projectCount " +
                    "FROM teams t LEFT JOIN project_team pt ON pt.team_id = t.team_id " +
                    "GROUP BY t.team_id " +
                    "ORDER BY 3 DESC")
    List<TeamProject> getTeamProjects();

}
