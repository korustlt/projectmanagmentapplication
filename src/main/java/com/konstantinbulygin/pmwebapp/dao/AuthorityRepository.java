package com.konstantinbulygin.pmwebapp.dao;

import com.konstantinbulygin.pmwebapp.entities.Authority;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий ролей
 */
@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Integer> {

    /**
     * Переопределенный метод findAll() из CrudRepository<T,T>
     * Возвращает список ролей из базы данных
     */
    @Override
    @NonNull
    List<Authority> findAll();

}
