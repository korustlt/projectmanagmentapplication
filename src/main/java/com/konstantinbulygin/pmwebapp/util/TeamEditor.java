package com.konstantinbulygin.pmwebapp.util;

import com.konstantinbulygin.pmwebapp.services.TeamService;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import java.util.Collection;

/**
 * Вспомогательный класс для преобразования к объекту UserAccount
 */
public class TeamEditor extends CustomCollectionEditor {

    /**
     * Поле сервиса UserAccountService
     */
    private final TeamService teamService;

    /**
     * Конструктор с параметрами
     */
    public TeamEditor(Class<? extends Collection> collectionType, TeamService teamService) {
        super(collectionType, true);
        this.teamService = teamService;
    }

    /**
     * Преобразует строку к id типа long.
     * Возвращает объект UserAccount найденный в базе данных по id
     */
    @Override
    protected Object convertElement(Object element) {
        if (element instanceof String) {
            long teamId = Long.parseLong((String) element);
            return teamService.findByTeamId(teamId);
        }
        return super.convertElement(element);
    }
}
