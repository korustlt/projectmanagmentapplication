package com.konstantinbulygin.pmwebapp.util;

import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import java.util.Collection;

/**
 * Вспомогательный класс для преобразования к объекту UserAccount
 */
public class UserAccountEditor extends CustomCollectionEditor {

    /**
     * Поле сервиса UserAccountService
     */
    private final UserAccountService userAccountService;

    /**
     * Конструктор с параметрами
     */
    public UserAccountEditor(Class<? extends Collection> collectionType, UserAccountService userAccountService) {
        super(collectionType, true);
        this.userAccountService = userAccountService;
    }

    /**
     * Преобразует строку к id типа long.
     * Возвращает объект UserAccount найденный в базе данных по id
     */
    @Override
    protected Object convertElement(Object element) {
        if (element instanceof String) {
            long userId = Long.parseLong((String) element);
            return userAccountService.findByUserId(userId);
        }
        return super.convertElement(element);
    }
}
