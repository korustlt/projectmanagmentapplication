package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.ProjectRepository;
import com.konstantinbulygin.pmwebapp.dto.ChartData;
import com.konstantinbulygin.pmwebapp.dto.TimeChartData;
import com.konstantinbulygin.pmwebapp.entities.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Сервис проектов
 */
@Service
public class ProjectService {

    /**
     * Поле репозитория проектов
     */
    private final ProjectRepository projectRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Получение списка проектов
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Получение списка данных для гистограммы
     */
    public List<ChartData> getProjectChartData() {
        return projectRepository.getProjectChartData();
    }

    /**
     * Получение списка данных для графика
     */
    public List<TimeChartData> getTimeData() {
        return projectRepository.getTimeData();
    }

    /**
     * Получение проекта по id
     */
    public Optional<Project> findById(long id) {
        return projectRepository.findById(id);
    }

    /**
     * Сохранение проекта
     */

    public Project save(Project project) {
        return projectRepository.save(project);
    }

    /**
     * Удаление проекта по id
     */
    public void deleteById(long id) {
        projectRepository.deleteById(id);
    }

    /**
     * Получение страницы проектов
     */
    public Page<Project> findProjectsPaginated(Pageable pageable) {
        // получение номера страницы
        int pageSize = pageable.getPageSize();
        // получение номера текущей страницы
        int currentPage = pageable.getPageNumber();
        // получение начального элемента на страницы
        int startItem = currentPage * pageSize;
        // получение списка проектов
        List<Project> allProjects = projectRepository.findAll();
        // объявление текущего списка проектов для наполнения страницы
        List<Project> currentProjects;
        // проверка условия, если список проектов пустой
        if (allProjects.size() < startItem) {
            // присваивание пустого списка в текущий список проектов
            currentProjects = Collections.emptyList();
        } else {
            // получение индекса для получения текущего списка из общего списка
            int toIndex = Math.min(startItem + pageSize, allProjects.size());
            // присваивание текущего списка из основного
            currentProjects = allProjects.subList(startItem, toIndex);
        }
        // возвращение текущего списка
        return new PageImpl<>(currentProjects, PageRequest.of(currentPage, pageSize), allProjects.size());
    }
}
