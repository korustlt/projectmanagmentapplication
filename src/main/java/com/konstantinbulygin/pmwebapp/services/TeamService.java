package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.TeamRepository;
import com.konstantinbulygin.pmwebapp.dto.TeamProject;
import com.konstantinbulygin.pmwebapp.entities.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Сервис команд
 */
@Service
public class TeamService {

    /**
     * Поле репозитория команд
     */
    private final TeamRepository teamRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    /**
     * Получение списка команд
     */
    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    /**
     * Получение команды по id
     */
    public Optional<Team> findById(long id) {
        return teamRepository.findById(id);
    }

    /**
     * Получение команды по id
     */
    public Team findByTeamId(long id) {
        return teamRepository.findByTeamId(id);
    }

    /**
     * Сохранение команды
     */
    public Team save(Team team) {
        return teamRepository.save(team);
    }

    /**
     * Удаление команды
     */
    @Transactional
    public void delete(long id) {
        teamRepository.deleteById(id);
    }

    /**
     * Получение страницы команд
     */
    public Page<Team> findTeamsPaginated(Pageable pageable) {

        // получение номера страницы
        int pageSize = pageable.getPageSize();
        // получение номера текущей страницы
        int currentPage = pageable.getPageNumber();
        // получение начального элемента на страницы
        int startItem = currentPage * pageSize;
        // получение списка команд
        List<Team> allTeams = teamRepository.findAll();
        // объявление текущего списка команд для наполнения страницы
        List<Team> currentTeams;
        // проверка условия, если список команд пустой
        if (allTeams.size() < startItem) {
            // присваивание пустого списка в текущий список команд
            currentTeams = Collections.emptyList();
        } else {
            // получение индекса для получения текущего списка из общего списка
            int toIndex = Math.min(startItem + pageSize, allTeams.size());
            // присваивание текущего списка из основного
            currentTeams = allTeams.subList(startItem, toIndex);
        }
        // возвращение текущего списка
        return new PageImpl<>(currentTeams, PageRequest.of(currentPage, pageSize), allTeams.size());
    }

    /**
     * Получение списка команд и проектов
     */
    public List<TeamProject> getTeamProjects() {
        return teamRepository.getTeamProjects();
    }


}
