package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dto.UserTeamDto;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы с аккаунтами пользователей используя репозиторий пользователей
 */
@Service
public class UserAccountService {

    /**
     * Поле репозитория пользователей
     */
    private final UserAccountDaoImpl userAccountRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public UserAccountService(UserAccountDaoImpl userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    /**
     * Сохранение пользователя
     */
    public UserAccount save(UserAccount userAccount) {
        return userAccountRepository.save(userAccount);
    }

    /**
     * Обновление пользователя
     */
    public UserAccount updateUser(UserAccount userAccount) {
        return userAccountRepository.updateUser(userAccount);
    }

    /**
     * Получение списка пользователей
     */
    public List<UserAccount> findAll() {
        return userAccountRepository.findAll();
    }

    /**
     * Получение пользователя по id
     */
    public UserAccount findByUserId(long id) {
        return userAccountRepository.findById(id);
    }

    /**
     * Получение пользователя по id
     */
    public Optional<UserAccount> findById(long id) {
        return Optional.of(userAccountRepository.findById(id));
    }

    /**
     * Удаление пользователя
     */
    @Transactional
    public void deleteById(long id) {
        userAccountRepository.deleteById(id);
    }

    /**
     * Получение списка команд и пользователей
     */
    public List<UserTeamDto> findTeamUsers() {
        return userAccountRepository.findTeamUsers();
    }

    /**
     * Получение страницы пользователей
     */
    public Page<UserAccount> findUsersPaginated(Pageable pageable) {

        // получение номера страницы
        int pageSize = pageable.getPageSize();
        // получение номера текущей страницы
        int currentPage = pageable.getPageNumber();
        // получение начального элемента на страницы
        int startItem = currentPage * pageSize;
        // получение списка пользователей
        List<UserAccount> userAccounts = userAccountRepository.findAll();
        // объявление текущего списка пользователей для наполнения страницы
        List<UserAccount> curUserAccounts;
        // проверка условия, если список пользователей пустой
        if (userAccounts.size() < startItem) {
            // присваивание пустого списка в текущий список пользователей
            curUserAccounts = Collections.emptyList();
        } else {
            // получение индекса для получения текущего списка из общего списка
            int toIndex = Math.min(startItem + pageSize, userAccounts.size());
            // присваивание текущего списка из основного
            curUserAccounts = userAccounts.subList(startItem, toIndex);
        }
        // возвращение текущего списка
        return new PageImpl<>(curUserAccounts, PageRequest.of(currentPage, pageSize), userAccounts.size());
    }

    /**
     * Поиск пользователя по логину
     */
    public Optional<UserAccount> findByUserLogin(String loginUser) {
        return userAccountRepository.findByUserLogin(loginUser);
    }
}

