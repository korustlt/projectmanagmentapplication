package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.TaskStatusRepository;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Сервис статусов
 */
@Service
public class TaskStatusService {

    /**
     * Поле репозитория статусов
     */
    private final TaskStatusRepository taskStatusRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public TaskStatusService(TaskStatusRepository taskStatusRepository) {
        this.taskStatusRepository = taskStatusRepository;
    }

    /**
     * Получение списка статусов
     */
    public List<TaskStatus> findAll() {
        return taskStatusRepository.findAll();
    }

    /**
     * Получение статуса по id
     */
    public Optional<TaskStatus> findById(long id) {
        return taskStatusRepository.findById(id);
    }

    /**
     * Сохранение статуса
     */
    public TaskStatus save(TaskStatus taskStatus) {
        return taskStatusRepository.save(taskStatus);
    }

    /**
     * Удаление статуса
     */
    @Transactional
    public void delete(long id) {
        taskStatusRepository.deleteById(id);
    }

    /**
     * Получение страницы статусов
     */
    public Page<TaskStatus> findStatusesPaginated(Pageable pageable) {

        // получение номера страницы
        int pageSize = pageable.getPageSize();
        // получение номера текущей страницы
        int currentPage = pageable.getPageNumber();
        // получение начального элемента на страницы
        int startItem = currentPage * pageSize;
        // получение списка статусов
        List<TaskStatus> taskStatuses = taskStatusRepository.findAll();
        // объявление текущего списка статусов для наполнения страницы
        List<TaskStatus> curTaskStatuses;
        // проверка условия, если список статусов пустой
        if (taskStatuses.size() < startItem) {
            // присваивание пустого списка в текущий список статусов
            curTaskStatuses = Collections.emptyList();
        } else {
            // получение индекса для получения текущего списка из общего списка
            int toIndex = Math.min(startItem + pageSize, taskStatuses.size());
            // присваивание текущего списка из основного
            curTaskStatuses = taskStatuses.subList(startItem, toIndex);
        }
        // возвращение текущего списка
        return new PageImpl<>(curTaskStatuses, PageRequest.of(currentPage, pageSize), taskStatuses.size());
    }


}
