package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.AuthorityRepository;
import com.konstantinbulygin.pmwebapp.entities.Authority;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Сервис ролей
 */
@Service
public class AuthorityService {

    /**
     * Поле репозитория ролей
     */
    private final AuthorityRepository authorityRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    /**
     * Получение роли по id
     */
    public Optional<Authority> findById(Integer id) {
        return authorityRepository.findById(id);
    }

    /**
     * Получение списка ролей
     */
    public List<Authority> findAll() {
        return authorityRepository.findAll();
    }
}
