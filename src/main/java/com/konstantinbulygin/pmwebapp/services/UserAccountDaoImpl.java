package com.konstantinbulygin.pmwebapp.services;


import com.konstantinbulygin.pmwebapp.dao.UserAccountDao;
import com.konstantinbulygin.pmwebapp.dto.UserTeamDto;
import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

/**
 * Репозиторий пользователей
 */
@Repository
public class UserAccountDaoImpl implements UserAccountDao {

    /**
     * Поле класса jdbcTemplate для выполнения запрос к базе данных
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Внедрение зависимости через конструктор
     */
    public UserAccountDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Производит поиск всех пользователей
     */
    @Override
    public List<UserAccount> findAll() {
        // запрос в базу данных
        String query = "SELECT * FROM user_accounts";
        // выполнение запроса и возврат результата
        return jdbcTemplate.query(query, (rs, rowNum) -> mapUserResult(rs));
    }

    /**
     * Производит поиск роли по имени роли
     */
    @Override
    public Authority findAuthorities(String name) {
        // запрос в базу данных
        String query =
                "SELECT authorities.authority_name " +
                        "FROM authorities " +
                        "WHERE authorities.authority_name = ?";
        // выполнение запроса и возврат результата
        return jdbcTemplate.queryForObject(query, (rs, rowNum) -> mapResultToAuthorities(rs), name);
    }

    /**
     * Производит поиск пользователей и команд
     */
    @Override
    public List<UserTeamDto> findTeamUsers() {
        // запрос в базу данных
        String query = "SELECT e.first_name AS firstName, e.last_name AS lastName, e.email , COUNT(pe.user_id) AS projectCount " +
                "FROM user_accounts e LEFT JOIN team_user pe ON pe.team_id = e.user_id " +
                "GROUP BY e.first_name, e.last_name, e.email " +
                "ORDER BY 4 DESC";
        // выполнение запроса и возврат результата
        return jdbcTemplate.query(query, (rs, rowNum) -> mapResultToUserTeam(rs));
    }

    /**
     * Производит поиск пользователя по id
     */
    @Override
    public UserAccount findById(long userId) {
        // запрос в базу данных
        String query = "SELECT * FROM user_accounts WHERE user_accounts.user_id = ?";
        // выполнение запроса и возврат результата
        return jdbcTemplate.queryForObject(query, (rs, rowNum) -> mapUserResult(rs), userId);
    }

    /**
     * Производит удаление пользователя по id
     */
    public boolean deleteById(long id) {
        // запрос в базу данных
        String query = "delete from user_accounts where user_id = ?";
        // выполнение запроса и возврат результата
        return jdbcTemplate.update(query, id) > 0;
    }

    /**
     * Производит поиск пользователя по логину
     */
    @Override
    public Optional<UserAccount> findByUserLogin(String userLogin) {
        // запрос в базу данных
        String query = "SELECT * FROM user_accounts WHERE user_accounts.user_login = ?";
        try {
            // выполнение запроса и возврат результата
            return Optional.ofNullable(jdbcTemplate.queryForObject(query, (rs, rowNum) -> mapUserResult(rs), userLogin));
        } catch (EmptyResultDataAccessException e) {
            // возврат пустого объекта
            return Optional.empty();
        }

    }

    /**
     * Сохраняет пользователя
     */
    @Override
    public UserAccount save(UserAccount user) {
        // объявление хранилища ключей для хранения результатов запроса в базу данных
        KeyHolder keyHolder = new GeneratedKeyHolder();
        // запрос в базу данных
        String query = "INSERT INTO user_accounts(email, first_name, last_name, password, role, dob, user_login) values(?,?,?,?,?,?,?)";
        // выполнение запроса
        jdbcTemplate.update(connection -> {
            // создание подготовленного запроса
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            // заполнение подготовленного запроса данными
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getFirstName());
            ps.setString(3, user.getLastName());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getRole());
            ps.setString(6, user.getUserDob().toString());
            ps.setString(7, user.getUserLogin());
            return ps;
        }, keyHolder);
        // получение id сохраненного пользователя
        BigInteger id = (BigInteger) keyHolder.getKey();
        // поиск по id и возврат найденного пользователя
        return findById(Long.parseLong(String.valueOf(id)));
    }

    @Override
    public UserAccount updateUser(UserAccount user) {
        // запрос в базу данных
        // UPDATE `mydb`.`user_accounts` SET `user_login` = 'plmoilfgfgfgfg' WHERE (`user_id` = '22');
        String query =
                "UPDATE user_accounts SET email = ?, first_name = ?, last_name = ?, " +
                        "password = ?, role = ?, dob = ?, user_login = ? " +
                        "WHERE (user_id = ?)";
        // выполнение запроса
        jdbcTemplate.update(connection -> {
            // создание подготовленного запроса
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            // заполнение подготовленного запроса данными
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getFirstName());
            ps.setString(3, user.getLastName());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getRole());
            ps.setString(6, user.getUserDob().toString());
            ps.setString(7, user.getUserLogin());
            ps.setLong(8, user.getUserId());
            return ps;
        });

        // поиск по id и возврат найденного пользователя
        return findById(user.getUserId());
    }

    /**
     * Преобразует результат запроса из базы данных к объекту Authority
     */
    private Authority mapResultToAuthorities(ResultSet rs) throws SQLException {
        // создание пустого объекта
        Authority authority = new Authority();
        // установка названия роли
        authority.setAuthorityName(rs.getString("authority_name"));
        // возврат объекта
        return authority;
    }

    /**
     * Преобразует результат запроса из базы данных к объекту UserTeamDto
     */
    private UserTeamDto mapResultToUserTeam(ResultSet rs) throws SQLException {
        // создание пустого объекта
        UserTeamDto userTeam = new UserTeamDto();
        // установка параметров объекта из результатов запроса
        userTeam.setFirstName(rs.getString("firstName"));
        // установка параметров объекта из результатов запроса
        userTeam.setLastName(rs.getString("lastName"));
        // установка параметров объекта из результатов запроса
        userTeam.setEmail(rs.getString("email"));
        // установка параметров объекта из результатов запроса
        userTeam.setProjectCount(rs.getInt("projectCount"));
        // возврат объекта
        return userTeam;
    }

    /**
     * Преобразует результат запроса из базы данных к объекту UserAccount
     */
    private UserAccount mapUserResult(final ResultSet rs) throws SQLException {
        // создание пустого объекта
        UserAccount user = new UserAccount();
        // установка параметров объекта из результатов запроса
        user.setUserId(rs.getLong("user_id"));
        // установка параметров объекта из результатов запроса
        user.setUserLogin(rs.getString("user_login"));
        // установка параметров объекта из результатов запроса
        user.setFirstName(rs.getString("first_name"));
        // установка параметров объекта из результатов запроса
        user.setLastName(rs.getString("last_name"));
        // установка параметров объекта из результатов запроса
        user.setEmail(rs.getString("email"));
        // установка параметров объекта из результатов запроса
        user.setPassword(rs.getString("password"));
        // установка параметров объекта из результатов запроса
        user.setUserDob(rs.getDate("dob").toLocalDate());
        // установка параметров объекта из результатов запроса
        user.setRole(rs.getString("role"));
        // возврат объекта
        return user;
    }
}
