package com.konstantinbulygin.pmwebapp.services;

import com.konstantinbulygin.pmwebapp.dao.ProjectTaskRepository;
import com.konstantinbulygin.pmwebapp.entities.ProjectTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Сервис задач для проектов
 */
@Service
public class ProjectTaskService {

    /**
     * Поле репозитория задач проектов
     */
    private final ProjectTaskRepository projectTaskRepository;

    /**
     * Внедрение зависимости через конструктор
     */
    public ProjectTaskService(ProjectTaskRepository projectTaskRepository) {
        this.projectTaskRepository = projectTaskRepository;
    }

    /**
     * Получение списка задач
     */
    public List<ProjectTask> findAll() {
        return projectTaskRepository.findAll();
    }

    /**
     * Получение задачи по id
     */
    public Optional<ProjectTask> findById(long id) {
        return projectTaskRepository.findById(id);
    }

    /**
     * Сохранение задачи
     */
    public ProjectTask save(ProjectTask projectTask) {
        return projectTaskRepository.save(projectTask);
    }

    /**
     * Удаление задачи
     */
    public void delete(long id) {
        projectTaskRepository.deleteById(id);
    }

    /**
     * Получение страницы задач
     */
    public Page<ProjectTask> findProjectTasksPaginated(Pageable pageable, long projectId) {

        // получение номера страницы
        int pageSize = pageable.getPageSize();
        // получение номера текущей страницы
        int currentPage = pageable.getPageNumber();
        // получение начального элемента на страницы
        int startItem = currentPage * pageSize;
        // получение списка задач
        List<ProjectTask> allProjectTask = projectTaskRepository.findByProjectId(projectId);
        // объявление текущего списка задач для наполнения страницы
        List<ProjectTask> currentProjectTask;
        // проверка условия, если список задач пустой
        if (allProjectTask.size() < startItem) {
            // присваивание пустого списка в текущий список задач
            currentProjectTask = Collections.emptyList();
        } else {
            // получение индекса для получения текущего списка из общего списка
            int toIndex = Math.min(startItem + pageSize, allProjectTask.size());
            // присваивание текущего списка из основного
            currentProjectTask = allProjectTask.subList(startItem, toIndex);
        }
        // возвращение текущего списка
        return new PageImpl<>(currentProjectTask, PageRequest.of(currentPage, pageSize), allProjectTask.size());
    }
}
