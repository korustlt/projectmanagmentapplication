package com.konstantinbulygin.pmwebapp.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konstantinbulygin.pmwebapp.dto.TimeChartData;
import com.konstantinbulygin.pmwebapp.entities.Project;
import com.konstantinbulygin.pmwebapp.entities.ProjectTask;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import com.konstantinbulygin.pmwebapp.entities.Team;
import com.konstantinbulygin.pmwebapp.services.*;
import com.konstantinbulygin.pmwebapp.util.TeamEditor;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Контроллер, обрабатывает запросы от пользователя
 */
@Controller
@RequestMapping("/projects")
@SuppressWarnings("unused")
public class ProjectController {

    /**
     * Преобразует данные поля введенные в форме на стороне клиента
     * к списку объектов UserAccount
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "teams", new TeamEditor(ArrayList.class, teamService));
    }

    /**
     * Внедрение объекта менеджера сущностей
     */
    @Autowired
    private EntityManager entityManager;

    /**
     * Поле логера
     */
    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    /**
     * Поле сервиса проектов
     */
    private final TeamService teamService;

    /**
     * Поле сервиса проектов
     */
    private final ProjectTaskService projectTaskService;

    /**
     * Поле сервиса проектов
     */
    private final ProjectService projectService;
    /**
     * Поле сервиса работников
     */
    private final UserAccountService userAccountService;

    /**
     * Поле сервиса работников
     */
    private final TaskStatusService taskStatusService;

    /**
     * Внедрение зависимостей через конструктор
     */
    public ProjectController(TeamService teamService, ProjectTaskService projectTaskService, ProjectService projectService, UserAccountService userAccountService, TaskStatusService taskStatusService) {
        this.teamService = teamService;
        this.projectTaskService = projectTaskService;
        this.projectService = projectService;
        this.userAccountService = userAccountService;
        this.taskStatusService = taskStatusService;
    }

    /**
     * Запрос для отображения списка проектов
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param size  Параметр для определения количества элементов на странице
     * @param page  Параметр для определения номера текущей страницы
     * @return Возвращает список проектов Project
     */
    @GetMapping()
    public String displayProjects(
            Model model,
            @RequestParam("size")
            Optional<Integer> size,
            @RequestParam("page")
            Optional<Integer> page) {

        // получение количества объектов на странице, по умолчанию 5
        int pageSize = size.orElse(5);
        // получение номера страницы
        int currentPage = page.orElse(1);
        // получение страницы с объектами Project
        Page<Project> projectPage = projectService.findProjectsPaginated(PageRequest.of(currentPage - 1, pageSize));
        // добавление атрибута projectPage и объекта projectPage в объект model
        model.addAttribute("projectPage", projectPage);
        // получение количества страниц в объекте userPage
        int totalPages = projectPage.getTotalPages();
        // объявление переменной для хранения количества страниц в списке
        List<Integer> pageNumbers = new ArrayList<>();
        // проверка условия, если количество страниц больше 0
        if (totalPages > 0) {
            // разбиение количества страниц на отдельные числа и запись в список
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        // добавление атрибута pageNumbers и объекта pageNumbers в объект model
        model.addAttribute("pageNumbers", pageNumbers);
        // получение списка статусов
        List<TaskStatus> allTaskStatuses = taskStatusService.findAll();
        // добавление атрибута allTaskStatuses и объекта allTaskStatuses в объект model
        model.addAttribute("allTaskStatuses", allTaskStatuses);
        // возвращает страницу со списком проектов
        return "projects/list-projects";
    }

    /**
     * Запрос для отображения формы для создания нового проекта
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/new")
    public String displayNewProjectForm(Model model) {
        // получение списка команд из базы данных
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);
        // получение списка статусов из базы данных
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // добавление атрибута project и пустого объекта Project в объект model
        model.addAttribute("project", new Project());
        // возвращает страницу с формой для добавления нового проекта
        return "projects/new-project";
    }

    /**
     * Запрос для сохранения данных проекта в базе данных
     *
     * @param project            Объект проекта Project
     * @param redirectAttributes Атрибуты перенаправления
     * @param model              Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @PostMapping("/save")
    @Transactional
    public String saveProject(
            @Valid
            @ModelAttribute("project")
            Project project,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Model model) {

        // проверка условия, если есть ошибки валидации
        if (bindingResult.hasErrors()) {
            logger.info("bindingResult = " + bindingResult);
            // получение списка статусов из базы данных
            List<TaskStatus> allStatuses = taskStatusService.findAll();
            // добавление атрибута allStatuses и объекта allStatuses в объект model
            model.addAttribute("allStatuses", allStatuses);
            // получение списка команд из базы данных
            List<Team> allTeams = teamService.findAll();
            // добавление атрибута allTeams и объекта allTeams в объект model
            model.addAttribute("allTeams", allTeams);
            // возвращает страницу добавления нового проекта
            return "projects/new-project";
        }

        // проверка условия, если время начало проекта больше времени окончания
        if (project.getStartDate().toEpochDay() > project.getEndDate().toEpochDay()) {
            // получение списка статусов из базы данных
            List<TaskStatus> allStatuses = taskStatusService.findAll();
            // добавление атрибута allStatuses и объекта allStatuses в объект model
            model.addAttribute("allStatuses", allStatuses);
            // получение списка команд из базы данных
            List<Team> allTeams = teamService.findAll();
            // добавление атрибута allTeams и объекта allTeams в объект model
            model.addAttribute("allTeams", allTeams);
            // добавление атрибута dateValidation и сообщения о необходимости выбора правильной даты
            model.addAttribute("dateValidation", "Выберите правильный диапазон дат");
            // возвращает страницу добавления нового проекта
            return "projects/new-project";
        }

        // проверка условия, если список команд пуст
        if (project.getTeams().isEmpty()) {
            // добавление атрибута employeeValidation и сообщения о необходимости выбора пользователя
            model.addAttribute("teamValidation", "Выберите одну или несколько команд");
            // получение списка статусов из базы данных
            List<TaskStatus> allStatuses = taskStatusService.findAll();
            // добавление атрибута allStatuses и объекта allStatuses в объект model
            model.addAttribute("allStatuses", allStatuses);
            // получение списка команд из базы данных
            List<Team> allTeams = teamService.findAll();
            // добавление атрибута allTeams и объекта allTeams в объект model
            model.addAttribute("allTeams", allTeams);
            // возвращение страницы добавления нового проекта
            return "projects/new-project";
        }

        // объявление нового списка пользователей
        List<Team> teams = new ArrayList<>();
        // проход в цикле по списку выбранных пользователей
        for (Team team : project.getTeams()) {
            // добавление найденных пользователей в список
            teams.add(reloadTeam(team.getTeamId()));
        }
        // добавление пользователей в список проекта
        project.setTeams(teams);

        // сохранение проекта в базу данных
        projectService.save(project);

        // добавление атрибута перенаправления message и сообщение для отображения на странице
        redirectAttributes.addFlashAttribute("message", "Проект добавлен");
        // добавление атрибута перенаправления alertClass, css класс для выделения сообщения цветом
        redirectAttributes.addFlashAttribute("alertClass", "alert-success");

        // перенаправление на страницу со списком проектов
        return "redirect:/projects";
    }

    /**
     * Вспомогательный метод для возврата объектов в текущий контекст транзакции
     *
     * @param teamId Идентификатор пользователя по которому производится поиск в базе данных
     */
    @Transactional
    public Team reloadTeam(Long teamId) {
        return entityManager.find(Team.class, teamId);
    }

    /**
     * Запрос для отображения формы для редактирования проекта
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param id    Идентификатор проекта по которому производится поиск в базе данных
     */
    @GetMapping("/edit/{id}")
    public String displayEditProject(@PathVariable long id, Model model) {
        // получение списка статусов
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // получение списка команд из базы данных
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);
        // получение проекта из базы данных по id
        Optional<Project> project = projectService.findById(id);
        // добавление атрибута project и объекта project в объект model
        model.addAttribute("project", project.orElse(null));
        // возвращение страницы редактирования проекта
        return "projects/edit-project";
    }

    /**
     * Запрос для удаления проекта используя id
     *
     * @param id Идентификатор проекта по которому производится поиск в базе данных
     */
    @GetMapping("/delete/{id}")
    public String deleteProject(@PathVariable long id, Model model) {

        try {
            // удаление проекта в базе данных по id
            projectService.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            model.addAttribute("deleteRestrictionProject", "Нельзя удалить этот проект");
            // перенаправляет на страницу ошибки
            return "security/error";
        }
        // перенаправление на страницу со списком проектов
        return "redirect:/projects";
    }


    /**
     * Запрос для отображения списка задач для проекта
     *
     * @param id    id проекта
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param size  Параметр для определения количества элементов на странице
     * @param page  Параметр для определения номера текущей страницы
     * @return Возвращает список задач ProjectTask для проекта
     */
    @GetMapping("/show/tasks/{id}")
    public String displayProjectTasks(
            @PathVariable
            long id,
            Model model,
            @RequestParam("size")
            Optional<Integer> size,
            @RequestParam("page")
            Optional<Integer> page
    ) {

        // получение количества объектов на странице, по умолчанию 5
        int pageSize = size.orElse(5);
        // получение номера страницы
        int currentPage = page.orElse(1);
        // получение страницы с объектами ProjectTask
        Page<ProjectTask> projectTasksPage = projectTaskService.findProjectTasksPaginated(PageRequest.of(currentPage - 1, pageSize), id);
        // добавление атрибута projectTasksPage и объекта projectTasks в объект model
        model.addAttribute("projectTasksPage", projectTasksPage);
        // получение количества страниц в объекте projectTasksPage
        int totalPages = projectTasksPage.getTotalPages();
        // объявление переменной для хранения количества страниц в списке
        List<Integer> pageNumbers = new ArrayList<>();
        // проверка условия, если количество страниц больше 0
        if (totalPages > 0) {
            // разбиение количества страниц на отдельные числа и запись в список
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        // добавление атрибута pageNumbers и объекта pageNumbers в объект model
        model.addAttribute("pageNumbers", pageNumbers);
        // получение проекта по id
        Optional<Project> project = projectService.findById(id);
        // добавление атрибута project и объекта project в объект model
        model.addAttribute("project", project.orElse(null));
        // получение списка команд
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);
        // получение списка статусов
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // возвращение страницы со списком задач для проекта
        return "projects/list-tasks";
    }


    /**
     * Запрос для отображения формы для создания новой задачи для проекта
     *
     * @param id    id проекта
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/add/task/{id}")
    public String displayAddTaskToProjectForm(@PathVariable long id, Model model) {
        // получение списка проектов из базы данных
        List<Project> allProjects = projectService.findAll();
        // добавление атрибута allProjects и объекта allProjects в объект model
        model.addAttribute("allProjects", allProjects);
        // получение списка статусов из базы данных
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // получение списка команд из базы данных
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);
        // добавление атрибута task и пустого объекта ProjectTask в объект model
        model.addAttribute("task", new ProjectTask());
        // возвращение страницы создания новой задачи
        return "projects/new-task";
    }

    /**
     * Запрос для сохранения задачи в базе данных
     *
     * @param task               Объект задачи ProjectTask
     * @param bindingResult      Результат связывания данных формы, полей объекта и их валидация
     * @param redirectAttributes Атрибуты перенаправления
     * @param model              Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @PostMapping("/task/save")
    public String saveTaskProject(
            @Valid
            @ModelAttribute("task")
            ProjectTask task,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Model model) {

        // проверка условия, если есть ошибки валидации перенаправит на страницу создания задачи
        if (bindingResult.hasErrors()) {
            // логирование ошибок
            logger.info("bindingResult = " + bindingResult);
            // получение списка статусов из базы данных
            List<Project> allProjects = projectService.findAll();
            // добавление атрибута allProjects и объекта allProjects в объект model
            model.addAttribute("allProjects", allProjects);
            // получение списка статусов из базы данных
            List<TaskStatus> allStatuses = taskStatusService.findAll();
            // добавление атрибута allStatuses и объекта allStatuses в объект model
            model.addAttribute("allStatuses", allStatuses);
            // получение списка команд из базы данных
            List<Team> allTeams = teamService.findAll();
            // добавление атрибута allTeams и объекта allTeams в объект model
            model.addAttribute("allTeams", allTeams);
            // возвращение страницы создания новой задачи
            return "admin/edit-task";
        }

        // добавление атрибута перенаправления message и сообщение для отображения на странице
        redirectAttributes.addFlashAttribute("message", "Задача добавлена");
        // добавление атрибута перенаправления alertClass и css класс для выделения сообщения цветом
        redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        // сохранение проекта в базу данных
        projectTaskService.save(task);
        // перенаправляем на страницу со списком проектов
        return "redirect:/projects";
    }


    /**
     * Запрос для отображения формы для редактирования задачи
     *
     * @param id    Идентификатор задачи по которому производится поиск в базе данных
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/edit/task/{id}")
    public String displayEditTaskProject(@PathVariable long id, Model model) {

        // получение задачи по id из базы данных
        Optional<ProjectTask> task = projectTaskService.findById(id);
        // добавление атрибута task и объекта task в объект model
        model.addAttribute("task", task.get());
        // получение списка статусов из базы данных
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // получение списка команд из базы данных
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);
        // получение списка проектов из базы данных
        List<Project> allProjects = projectService.findAll();
        // добавление атрибута allProjects и объекта allProjects в объект model
        model.addAttribute("allProjects", allProjects);
        // возвращение страницы редактирования задачи
        return "admin/edit-task";
    }

    /**
     * Запрос для удаления задачи используя id
     *
     * @param id Идентификатор задачи по которому производится поиск в базе данных
     */
    @GetMapping("/delete/task/{id}")
    public String deleteTask(@PathVariable long id) {
        // удаление задачи в базе данных по id
        projectTaskService.delete(id);
        // перенаправление на страницу со списком проектов
        return "redirect:/projects";
    }


    /**
     * Запрос для отображения графика выполнения проектов
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/timelines")
    public String displayProjectTimelines(Model model) throws JsonProcessingException {

        // получение списка данных для проектов из базы данных
        List<TimeChartData> timeLineData = projectService.getTimeData();

        if (timeLineData.isEmpty()) {
            // добавление атрибута projectTimeList и объекта jsonTimeLine в объект model
            model.addAttribute("projectListIsEmpty", "Проектов пока нет");
            // возвращение страницы с графиком
            return "projects/project-timelines";
        } else {
            // создание маппера
            ObjectMapper objectMapper = new ObjectMapper();
            // получение json строки из timeLineData
            String jsonTimeLine = objectMapper.writeValueAsString(timeLineData);
            // добавление атрибута projectTimeList и объекта jsonTimeLine в объект model
            model.addAttribute("projectTimeList", jsonTimeLine);
            // возвращение страницы с графиком
            return "projects/project-timelines";
        }
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
