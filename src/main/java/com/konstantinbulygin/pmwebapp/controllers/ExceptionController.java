package com.konstantinbulygin.pmwebapp.controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Обрабатывает ошибки, которые происходят в приложении
 */
@Controller
@SuppressWarnings("unused")
public class ExceptionController implements ErrorController {

    /**
     * Запрос для отображения ошибки
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param request  Параметр представляет объект запроса со статусами ошибки
     */
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        // получает статус ошибки из запроса
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        // добавление атрибута status и объекта status в объект model
        model.addAttribute("status", status);
        // возвращает страницу с ошибкой
        return "security/error";
    }
}
