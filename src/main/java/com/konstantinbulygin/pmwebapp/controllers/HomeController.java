package com.konstantinbulygin.pmwebapp.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konstantinbulygin.pmwebapp.dto.ChartData;
import com.konstantinbulygin.pmwebapp.dto.TeamProject;
import com.konstantinbulygin.pmwebapp.entities.Project;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import com.konstantinbulygin.pmwebapp.entities.Team;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.ProjectService;
import com.konstantinbulygin.pmwebapp.services.TaskStatusService;
import com.konstantinbulygin.pmwebapp.services.TeamService;
import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * Контролер, обрабатывает запросы от любого пользователя
 */
@Controller
@RequestMapping("/")
@SuppressWarnings("unused")
public class HomeController {

    /**
     * Поле логера
     */
    private final Logger logger = LoggerFactory.getLogger(HomeController.class);

    /**
     * Поле сервиса аккаунта пользователей
     */
    private final UserAccountService userAccountService;

    /**
     * Поле сервиса аккаунта пользователей
     */
    private final TaskStatusService taskStatusService;

    /**
     * Поле сервиса аккаунта пользователей
     */
    private final TeamService teamService;

    /**
     * Поле сервиса проектов
     */
    private final ProjectService projectService;

    /**
     * Внедрение зависимостей через конструктор
     */
    public HomeController(UserAccountService userAccountService, TaskStatusService taskStatusService, TeamService teamService, ProjectService projectService) {
        this.userAccountService = userAccountService;
        this.taskStatusService = taskStatusService;
        this.teamService = teamService;
        this.projectService = projectService;
    }

    /**
     * Запрос для отображения главной страницы
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/")
    public String displayMainView(Model model) throws JsonProcessingException {

        // получение списка проектов из базы данных
        List<Project> projects = projectService.findAll();
        // добавление атрибута projectsList и объекта projects в объект model
        model.addAttribute("projectsList", projects);

        // получение списка пользователей и проектов из базы данных
        List<TeamProject> teamProject = teamService.getTeamProjects();
        // добавление атрибута employeeProjects и объекта userProject в объект model
        model.addAttribute("teamProject", teamProject);

        // получение списка команд из базы данных
        List<Team> allTeams = teamService.findAll();
        // добавление атрибута allTeams и объекта allTeams в объект model
        model.addAttribute("allTeams", allTeams);

        // получение списка статусов из базы данных
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);

        // получение списка пользователей из базы данных
        List<UserAccount> allUsers = userAccountService.findAll();
        // добавление атрибута allUsers и объекта allUsers в объект model
        model.addAttribute("allUsers", allUsers);

        // получение списка проектов для построения гистограммы из базы данных
        List<ChartData> projectData = projectService.getProjectChartData();
        // создание маппера
        ObjectMapper objectMapper = new ObjectMapper();
        // конвертация списка проектов в json
        String json = objectMapper.writeValueAsString(projectData);
        // добавление атрибута projectStatusCnt и объекта json в объект model
        model.addAttribute("projectStatusCnt", json);
        // возвращает страницу со все информацией
        return "main/projects-info";
    }

    /**
     * Запрос для отображения всех статусов с использованием Ajax запроса
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping(value = "/getAllStatuses")
    public String getAllStatusesUsingAjax(Model model) {
        // получение списка статусов из базы данных
        List<TaskStatus> allStatuses = taskStatusService.findAll();
        // добавление атрибута allStatuses и объекта allStatuses в объект model
        model.addAttribute("allStatuses", allStatuses);
        // возвращает фрагмент с информацией о статусах
        return "fragments/ajaxPart :: all-statuses";
    }
}
