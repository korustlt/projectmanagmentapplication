package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.AuthorityService;
import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Контроллер безопасности, обрабатывает запросы от пользователя,
 * позволяет регистрировать нового пользователя или зайти на сайт уже зарегистрированному пользователю
 */
@Controller
@SuppressWarnings("unused")
public class SecurityController {

    /**
     * Поле логера
     */
    private final Logger logger = LoggerFactory.getLogger(SecurityController.class);

    /**
     * Поле кодировщика паролей
     */
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    /**
     * Поле сервиса аккаунта пользователей
     */
    private final UserAccountService userAccountService;

    /**
     * Поле сервиса ролей
     */
    private final AuthorityService authorityService;

    /**
     * Внедрение зависимостей через конструктор
     */
    public SecurityController(BCryptPasswordEncoder bCryptPasswordEncoder, UserAccountService userAccountService, AuthorityService authorityService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userAccountService = userAccountService;
        this.authorityService = authorityService;
    }

    /**
     * Запрос для отображения страницы с формой регистрации нового пользователя
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/register")
    public String register(Model model) {
        // добавление атрибута userAccount и пустого объекта UserAccount в объект model
        model.addAttribute("userAccount", new UserAccount());
        // получение списка ролей
        List<Authority> allAuthorities = authorityService.findAll();
        // добавление атрибута allAuthorities и объекта allAuthorities в объект model
        model.addAttribute("allAuthorities", allAuthorities);
        // возвращение страницы с формой для регистрации
        return "security/register";
    }

    /**
     * Запрос для сохранения данных пользователя в базе данных
     *
     * @param model         Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param userAccount   Объект пользователя
     * @param bindingResult Результат связывания данных формы, полей объекта и их валидация
     */
    @PostMapping("/register/save")
    public String saveUser(Model model, @Valid UserAccount userAccount, BindingResult bindingResult) {

        // проверка условия, если есть ошибки валидации, то будет перенаправление на страницу регистрации
        if (bindingResult.hasErrors()) {
            // логирование ошибок
            logger.info("bindingResult = " + bindingResult);
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и объекта allAuthorities в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы регистрации
            return "security/register";
        }

        // проверка условия, если введенный пароль не совпадает с подтвержденным паролем, то будет перенаправление на страницу регистрации
        if (!userAccount.getPassword().equals(userAccount.getConfirmPassword())) {
            // добавление сообщения с информацией, что пароли не совпадают
            model.addAttribute("passwordMathProblem", "Пароли не совпадают");
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы с формой для регистрации нового пользователя и описанием ошибки
            return "security/register";
        }

        // получение объекта пользователя из базы данных по логину пользователя
        Optional<UserAccount> user = userAccountService.findByUserLogin(userAccount.getUserLogin());

        // проверка условия, если такой объект найден, то будет перенаправление на страницу регистрации
        if (user.isPresent()) {
            // добавление атрибута createUserError и сообщения с информацией, что пользователь с таким логином уже есть
            model.addAttribute("createUserError", "Пользователь с таким логином уже есть");
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы с формой для регистрации нового пользователя и описанием ошибки
            return "security/register";
        }

        // кодирование пароля и установка пароля пользователю
        userAccount.setPassword(bCryptPasswordEncoder.encode(userAccount.getPassword()));
        // получение списка ролей
        List<Authority> authorities = authorityService.findAll();
        // получение роли для пользователя
        Authority userAuthority = userAccount.getAuthorities().get(0);
        // установка названия роли для пользователя
        for (Authority authority : authorities) {
            if (Objects.equals(authority.getAuthorityId(), userAuthority.getAuthorityId())) {
                userAccount.setRole(userAccount.getAuthorities().get(0).getAuthorityName());
            }
        }

        // сохранение аккаунта пользователя в базе данных
        userAccountService.save(userAccount);
        // перенаправление на страницу входа
        return "redirect:/login";
    }

    /**
     * Запрос для входа зарегистрированным пользователям
     */
    @GetMapping("/login")
    public String displayLoginForm() {
        // возвращение страницы входа
        return "security/login";
    }
}
