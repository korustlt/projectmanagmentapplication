package com.konstantinbulygin.pmwebapp.controllers;

import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.TaskStatus;
import com.konstantinbulygin.pmwebapp.entities.Team;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.AuthorityService;
import com.konstantinbulygin.pmwebapp.services.TaskStatusService;
import com.konstantinbulygin.pmwebapp.services.TeamService;
import com.konstantinbulygin.pmwebapp.services.UserAccountService;
import com.konstantinbulygin.pmwebapp.util.UserAccountEditor;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Обрабатывает запросы от пользователя который имеет роль администратора
 */
@Controller
@RequestMapping("/admin")
@SuppressWarnings("unused")
public class AdminController {

    /**
     * Поле логера
     */
    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    /**
     * Преобразует данные поля введенные в форме на стороне клиента
     * к списку объектов UserAccount
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "teamUsers", new UserAccountEditor(ArrayList.class, userAccountService));
    }

    /**
     * Внедрение объекта менеджера сущностей
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Поле сервиса ролей
     */
    private final AuthorityService authorityService;
    /**
     * Поле сервиса аккаунта пользователей
     */
    private final UserAccountService userAccountService;
    /**
     * Поле сервиса статуса задач
     */
    private final TaskStatusService taskStatusService;

    /**
     * Поле сервиса команд
     */
    private final TeamService teamService;

    /**
     * Поле кодировщика паролей
     */
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Внедрение зависимостей через конструктор
     */
    public AdminController(AuthorityService authorityService, UserAccountService userAccountService, TaskStatusService taskStatusService, TeamService teamService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.authorityService = authorityService;
        this.userAccountService = userAccountService;
        this.taskStatusService = taskStatusService;
        this.teamService = teamService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Запрос для отображения списка пользователей приложения
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param size  Параметр для определения количества элементов на странице
     * @param page  Параметр для определения номера текущей страницы
     * @return Возвращает список пользователей UserAccount
     */
    @GetMapping("/users")
    public String displayUsers(
            Model model,
            @RequestParam("size")
            Optional<Integer> size,
            @RequestParam("page")
            Optional<Integer> page) {

        // получение количества объектов на странице, по умолчанию 5
        int pageSize = size.orElse(5);
        // получение номера страницы
        int currentPage = page.orElse(1);
        // получение страницы с объектами UserAccount
        Page<UserAccount> userPage = userAccountService.findUsersPaginated(PageRequest.of(currentPage - 1, pageSize));
        // добавление атрибута userPage и объекта userPage в объект model
        model.addAttribute("userPage", userPage);
        // получение количества страниц в объекте userPage
        int totalPages = userPage.getTotalPages();
        // объявление переменной для хранения количества страниц в списке
        List<Integer> pageNumbers = new ArrayList<>();
        // проверка условия, если количество страниц больше 0
        if (totalPages > 0) {
            // разбиение количества страниц на отдельные числа и запись в список
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        // добавление атрибута pageNumbers и объекта pageNumbers в объект model
        model.addAttribute("pageNumbers", pageNumbers);
        // возвращение страницы со списком пользователей
        return "admin/list-users";
    }

    /**
     * Запрос для отображения формы для регистрации нового пользователя
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/user/new")
    public String displayUserForm(Model model) {
        // добавление атрибута userAccount и пустого объекта UserAccount в объект model
        model.addAttribute("userAccount", new UserAccount());
        // получение списка ролей
        List<Authority> allAuthorities = authorityService.findAll();
        // добавление атрибута allAuthorities и списка ролей в объект model
        model.addAttribute("allAuthorities", allAuthorities);
        // возвращение страницы с формой для регистрации нового пользователя
        return "users/new-user";
    }

    /**
     * Запрос для сохранения данных пользователя в базе данных
     *
     * @param model         Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param userAccount   Объект пользователя
     * @param bindingResult Результат связывания данных формы, полей объекта и их валидация
     */
    @PostMapping("/user/save")
    public String saveUser(
            Model model,
            @Valid
            UserAccount userAccount,
            BindingResult bindingResult
    ) {
        // проверка условия, если есть ошибки валидации, то будет перенаправление на страницу регистрации нового пользователя
        if (bindingResult.hasErrors()) {
            // логирование ошибок
            logger.info("bindingResult = " + bindingResult);
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы с формой для регистрации нового пользователя и описанием ошибки
            return "users/new-user";
        }

        // проверка условия, если введенный пароль не совпадает с подтвержденным паролем, то будет перенаправление на страницу регистрации
        if (!userAccount.getPassword().equals(userAccount.getConfirmPassword())) {
            // добавление сообщения с информацией, что пароли не совпадают
            model.addAttribute("passwordMathProblem", "Пароли не совпадают");
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы с формой для регистрации нового пользователя и описанием ошибки
            return "users/new-user";
        }

        // получение объекта пользователя из базы данных по логину пользователя
        Optional<UserAccount> user = userAccountService.findByUserLogin(userAccount.getUserLogin());
        // проверка условия, если такой объект найден, то будет перенаправление на страницу регистрации
        if (user.isPresent()) {
            // добавление атрибута createUserError и сообщения с информацией, что пользователь с таким логином уже есть
            model.addAttribute("createUserError", "Пользователь с таким логином уже есть");
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // возвращение страницы с формой для регистрации нового пользователя и описанием ошибки
            return "users/new-user";
        }

        // кодирование пароля и установка пользователю
        userAccount.setPassword(bCryptPasswordEncoder.encode(userAccount.getPassword()));
        // получение списка ролей
        List<Authority> authorities = authorityService.findAll();
        // получение роли для пользователя
        Authority userAuthority = userAccount.getAuthorities().get(0);
        // установка названия роли для пользователя
        for (Authority authority : authorities) {
            if (Objects.equals(authority.getAuthorityId(), userAuthority.getAuthorityId())) {
                userAccount.setRole(userAccount.getAuthorities().get(0).getAuthorityName());
            }
        }
        // сохранение пользователя в базе данных
        userAccountService.save(userAccount);
        // перенаправление на страницу со списком пользователей
        return "redirect:/admin/users";
    }


    /**
     * Запрос для отображения формы для редактирования пользователя
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param id    Идентификатор пользователя по которому производится поиск в базе данных
     */
    @GetMapping("/edit/{id}")
    public String displayEditUsersForm(@PathVariable long id, Model model) {
        // поиск пользователя в базе данных по id
        UserAccount userAccount = userAccountService.findByUserId(id);
        // добавление атрибута userAccount и объекта userAccount в объект model
        model.addAttribute("userAccount", userAccount);
        // получение списка ролей
        List<Authority> allAuthorities = authorityService.findAll();
        // добавление атрибута allAuthorities и списка ролей в объект model
        model.addAttribute("allAuthorities", allAuthorities);
        // возвращение страницы с пользователем для редактирования
        return "admin/edit-user";
    }

    /**
     * Запрос для сохранения редактированного пользователя в базу данных
     *
     * @param model         Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param userAccount   Объект пользователя
     * @param bindingResult Результат связывания данных формы, полей объекта и их валидация
     */
    @PostMapping("/edit/save")
    public String saveUser(Model model, @Valid UserAccount userAccount, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        // проверка условия, если есть ошибки валидации, то будет перенаправление на страницу регистрации нового пользователя
        if (bindingResult.hasErrors()) {
            // логирует результат связывания полей формы с полями объекта
            logger.info("bindingResult = " + bindingResult);
            // получение списка ролей
            List<Authority> allAuthorities = authorityService.findAll();
            // добавление атрибута allAuthorities и списка ролей в объект model
            model.addAttribute("allAuthorities", allAuthorities);
            // перенаправляет на страницу редактирования
            return "admin/edit-user";
        }

        // получение списка ролей
        List<Authority> authorities = authorityService.findAll();
        // получение роли для пользователя
        Authority art = userAccount.getAuthorities().get(0);
        // установка названия роли для пользователя
        for (Authority authority : authorities) {
            if (Objects.equals(authority.getAuthorityId(), art.getAuthorityId())) {
                userAccount.setRole(userAccount.getAuthorities().get(0).getAuthorityName());
            }
        }
        // добавляем атрибуты перенаправления, сообщение для отображения на странице
        redirectAttributes.addFlashAttribute("message", "Пользователь добавлен");
        // добавляем атрибуты перенаправления, css класс для выделения сообщения цветом
        redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        // сохраняет пользователя в базу данных
        userAccountService.updateUser(userAccount);
        // перенаправляет на страницу пользователей
        return "redirect:/admin/users";
    }

    /**
     * Запрос для удаления пользователя используя id
     *
     * @param id Идентификатор пользователя по которому производится поиск в базе данных
     */
    @GetMapping("/delete/{id}")
    public String deleteUserById(@PathVariable long id, Model model) {

        try {
            // удаление пользователя в базе данных по id
            userAccountService.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            model.addAttribute("deleteRestrictionUser", "Нельзя удалить этого пользователя,он находится в команде");
            // перенаправляет на страницу ошибки
            return "security/error";
        }
        // перенаправление на страницу пользователей
        return "redirect:/admin/users";
    }


    /**
     * Запрос для отображения списка статусов
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param size  Параметр для определения количества элементов на странице
     * @param page  Параметр для определения номера текущей страницы
     * @return Возвращает список статусов TaskStatus
     */
    @GetMapping("/statuses")
    public String displayStatuses(
            Model model,
            @RequestParam("size")
            Optional<Integer> size,
            @RequestParam("page")
            Optional<Integer> page) {

        // получение количества объектов на странице, по умолчанию 5
        int pageSize = size.orElse(5);
        // получение номера страницы
        int currentPage = page.orElse(1);
        // получение страницы с объектами TaskStatus
        Page<TaskStatus> statusPage = taskStatusService.findStatusesPaginated(PageRequest.of(currentPage - 1, pageSize));
        // добавление атрибута statusPage и объекта statusPage в объект model
        model.addAttribute("statusPage", statusPage);
        // получение количества страниц в объекте userPage
        int totalPages = statusPage.getTotalPages();
        // объявление переменной для хранения количества страниц в списке
        List<Integer> pageNumbers = new ArrayList<>();
        // проверка условия, если количество страниц больше 0
        if (totalPages > 0) {
            // разбиение количества страниц на отдельные числа и запись в список
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        // добавление атрибута pageNumbers и объекта pageNumbers в объект model
        model.addAttribute("pageNumbers", pageNumbers);
        // возвращаем страницу со списком статусов
        return "admin/list-statuses";
    }

    /**
     * Запрос для отображения формы для создания нового статуса
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/status/new")
    public String displayStatusForm(Model model) {
        // создание пустого объекта TaskStatus
        TaskStatus statusNew = new TaskStatus();
        // добавляет пустой объект статуса в объект model
        model.addAttribute("statusNew", statusNew);
        // возвращает страницу с формой для создания нового статуса
        return "statuses/new-status";
    }

    /**
     * Запрос для сохранения данных статуса в базе данных
     *
     * @param taskStatus    Объект статуса TaskStatus
     * @param bindingResult Результат связывания данных формы, полей объекта и их валидация
     * @param model         Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @PostMapping("/status/save")
    public String saveStatus(
            @Valid
            @ModelAttribute("statusNew")
            TaskStatus taskStatus,
            BindingResult bindingResult,
            Model model) {
        // проверка условия, если есть ошибки валидации, то будет перенаправление на страницу создания нового статуса
        if (bindingResult.hasErrors()) {
            // перенаправляет на страницу создания нового статуса
            return "statuses/new-status";
        }
        // получает массив символов из названия статуса
        char[] chars = taskStatus.getStatusName().toCharArray();

        // запускает цикл
        for (char c : chars) {
            // проверяет условие, если символ это цифра
            if (Character.isDigit(c)) {
                // создает объект ошибки с описанием ошибки
                ObjectError error = new ObjectError("digitInStatus", "Статус содержит цифры");
                // добавляет ошибку в результаты связывания
                bindingResult.addError(error);
                // добавляет атрибут digitInStatus и сообщение в объект model
                model.addAttribute("digitInStatus", "Статус содержит цифры");
                // перенаправляет на страницу создания нового статуса
                return "statuses/new-status";
            }
        }

        // сохраняет статус в базе данных
        taskStatusService.save(taskStatus);
        // перенаправляет на страницу статусов
        return "redirect:/admin/statuses";
    }


    /**
     * Запрос для отображения формы для редактирования статуса
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param id    Идентификатор статуса по которому производится поиск в базе данных
     */
    @GetMapping("/status/edit/{id}")
    public String displayEditStatusForm(@PathVariable long id, Model model) {
        // поиск статуса в базе данных по id
        Optional<TaskStatus> taskStatus = taskStatusService.findById(id);
        // добавляет атрибут status и объект taskStatus в объект model
        model.addAttribute("status", taskStatus.orElse(null));
        // возвращает страницу для редактирования статуса
        return "admin/edit-status";
    }

    /**
     * Запрос для удаления статуса используя id
     *
     * @param id Идентификатор статуса по которому производится поиск в базе данных
     */
    @GetMapping("/status/delete/{id}")
    public String deleteStatusById(Model model, @PathVariable long id) {
        try {
            // удаление статуса в базе данных по id
            taskStatusService.delete(id);
        } catch (DataIntegrityViolationException e) {
            model.addAttribute("deleteRestrictionStatus", "Нельзя удалить этот статус, так как он используется");
            // перенаправляет на страницу статусов
            return "security/error";
        }
        // перенаправляет на страницу статусов
        return "redirect:/admin/statuses";
    }

    /**
     * Запрос для отображения списка команд
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param size  Параметр для определения количества элементов на странице
     * @param page  Параметр для определения номера текущей страницы
     * @return Возвращает список команд Teams
     */
    @GetMapping("/teams")
    public String displayTeams(
            Model model,
            @RequestParam("size")
            Optional<Integer> size,
            @RequestParam("page")
            Optional<Integer> page) {

        // получение количества объектов на странице, по умолчанию 5
        int pageSize = size.orElse(5);
        // получение номера страницы
        int currentPage = page.orElse(1);
        // получение страницы с объектами Team
        Page<Team> teamPage = teamService.findTeamsPaginated(PageRequest.of(currentPage - 1, pageSize));
        // добавление атрибута teamPage и объекта teamPage в объект model
        model.addAttribute("teamPage", teamPage);
        // получение количества страниц в объекте teamPage
        int totalPages = teamPage.getTotalPages();
        // объявление переменной для хранения количества страниц в списке
        List<Integer> pageNumbers = new ArrayList<>();
        // проверка условия, если количество страниц больше 0
        if (totalPages > 0) {
            // разбиение количества страниц на отдельные числа и запись в список
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        // добавление атрибута pageNumbers и объекта pageNumbers в объект model
        model.addAttribute("pageNumbers", pageNumbers);
        // возвращаем страницу со списком команд
        return "admin/list-teams";
    }

    /**
     * Запрос для отображения формы для создания новой команды
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @GetMapping("/team/new")
    public String displayTeamForm(Model model) {
        // добавление атрибута team и пустого объекта Team в объект model
        model.addAttribute("team", new Team());
        // получение списка всех пользователей
        List<UserAccount> allUsers = userAccountService.findAll();
        // добавление атрибута allUsers и объекта allUsers в объект model
        model.addAttribute("allUsers", allUsers);
        // возвращает страницу с формой для добавления команды
        return "teams/new-team";
    }

    /**
     * Запрос для сохранения данных команды в базе данных
     *
     * @param team          Объект команды Team
     * @param bindingResult Результат связывания данных формы, полей объекта и их валидация
     * @param model         Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     */
    @PostMapping("/team/save")
    public String saveTeam(
            @Valid
            @ModelAttribute("team")
            Team team,
            BindingResult bindingResult,
            Model model) {
        // проверка условия, если есть ошибки валидации, то будет перенаправление на страницу создания новой команды
        if (bindingResult.hasErrors()) {
            // логирование ошибок
            logger.info("bindingResult = " + bindingResult);
            // получение списка всех пользователей
            List<UserAccount> allUsers = userAccountService.findAll();
            // добавление атрибута allUsers и объекта allUsers в объект model
            model.addAttribute("allUsers", allUsers);
            // возвращает страницу с формой для добавления команды
            return "teams/new-team";
        }

        // проверка условия, если пользователей в команде равно 0, то будет перенаправление на страницу создания новой команды
        if (team.getTeamUsers().isEmpty()) {
            // получение списка всех пользователей
            List<UserAccount> allUsers = userAccountService.findAll();
            // добавление атрибута allUsers и объекта allUsers в объект model
            model.addAttribute("allUsers", allUsers);
            // добавление атрибута emptyUserList и сообщения в объект model
            model.addAttribute("emptyUserList", "Выберите одного или несколько пользователей");
            // возвращает страницу с формой для добавления команды
            return "teams/new-team";
        }

        // получает массив символов из названия команды
        char[] chars = team.getTeamName().toCharArray();

        // запускает цикл
        for (char c : chars) {
            // проверяет условие, если символ это цифра
            if (Character.isDigit(c)) {
                // создает объект ошибки с описанием ошибки
                ObjectError error = new ObjectError("digitInTeamName", "Имя команды содержит цифры");
                // добавляет ошибку в результаты связывания
                bindingResult.addError(error);
                // добавляет атрибут digitInTeamName и сообщение в объект model
                model.addAttribute("digitInTeamName", "Имя команды содержит цифры");
                // получение списка всех пользователей
                List<UserAccount> allUsers = userAccountService.findAll();
                // добавление атрибута allUsers и объекта allUsers в объект model
                model.addAttribute("allUsers", allUsers);
                // возвращает страницу с формой для добавления команды
                return "teams/new-team";
            }
        }

        // объявление нового списка пользователей
        List<UserAccount> users = new ArrayList<>();
        // проход в цикле по списку выбранных пользователей
        for (UserAccount user : team.getTeamUsers()) {
            // добавление найденных пользователей в список
            users.add(reloadUserAccount(user.getUserId()));
        }
        // добавление пользователей в список команды
        team.setTeamUsers(users);

        // сохраняет команду в базе данных
        teamService.save(team);
        // перенаправляет на страницу со списком команд
        return "redirect:/admin/teams";
    }

    /**
     * Вспомогательный метод для возврата объектов в текущий контекст транзакции
     *
     * @param userId Идентификатор пользователя по которому производится поиск в базе данных
     */
    @Transactional
    public UserAccount reloadUserAccount(Long userId) {
        return entityManager.find(UserAccount.class, userId);
    }

    /**
     * Запрос для отображения формы для редактирования команды
     *
     * @param model Объект для добавления атрибутов и передачи их на view для шаблонизатора Thymeleaf
     * @param id    Идентификатор команды по которому производится поиск в базе данных
     */
    @GetMapping("/team/edit/{id}")
    public String displayEditTeamForm(@PathVariable long id, Model model) {
        // поиск команды в базе данных по id
        Optional<Team> team = teamService.findById(id);
        // добавление атрибута team и объекта team в объект model
        model.addAttribute("team", team.orElse(null));
        // получение списка всех пользователей
        List<UserAccount> allUsers = userAccountService.findAll();
        // добавление атрибута allUsers и объекта allUsers в объект model
        model.addAttribute("allUsers", allUsers);
        // возвращаем страницу с командой для редактирования
        return "admin/edit-team";
    }


    /**
     * Запрос для удаления команды используя id
     *
     * @param id Идентификатор команды по которому производится поиск в базе данных
     */
    @GetMapping("/team/delete/{id}")
    public String deleteTeamById(Model model, @PathVariable long id) {
        try {
            // удаление команды в базе данных по id
            teamService.delete(id);
        } catch (DataIntegrityViolationException e) {
            model.addAttribute("deleteRestrictionTeam", "Нельзя удалить эту команду, она участвует в проекте");
            // перенаправляет на страницу ошибки
            return "security/error";
        }
        // перенаправляет на страницу со списком команд
        return "redirect:/admin/teams";
    }
}
