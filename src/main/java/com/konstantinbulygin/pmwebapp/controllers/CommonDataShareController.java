package com.konstantinbulygin.pmwebapp.controllers;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;

/**
 * Предоставляет общую информацию для всего приложения
 */
@ControllerAdvice
@SuppressWarnings("unused")
public class CommonDataShareController {

    /**
     * Аннотация, которая привязывает параметр метода или возвращаемое методом значение к именованному атрибуту модели.
     * Поддерживается для классов контроллеров с методами @RequestMapping.
     */
    @ModelAttribute
    public void shareData(Principal principal, Model model) {
        if (principal != null) {
            // добавление атрибута principal и объекта principal в объект model
            model.addAttribute("principal", principal);
        }
    }
}

