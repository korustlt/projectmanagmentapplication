package com.konstantinbulygin.pmwebapp.dto;

import java.util.Date;


/**
 * Интерфейс для отображения графика по проектам в разрезе времени
 */
public interface TimeChartData {

    /**
     * Получение названия проекта
     */
    String getProjectName();

    /**
     * Получение даты начала проекта
     */
    Date getStartDate();

    /**
     * Получение даты окончания проекта
     */
    Date getEndDate();
}
