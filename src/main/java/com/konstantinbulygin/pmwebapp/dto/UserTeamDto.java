package com.konstantinbulygin.pmwebapp.dto;

public class UserTeamDto {

    /**
     * Поле имени
     */
    private String firstName;

    /**
     * Поле фамилии
     */
    private String lastName;

    /**
     * Поле почты
     */
    private String email;

    /**
     * Поле количества проектов
     */
    private int projectCount;

    /**
     * Конструктор без параметров
     */
    public UserTeamDto() {
    }

    /* геттеры и сеттеры */
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getProjectCount() {
        return projectCount;
    }

    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }
}
