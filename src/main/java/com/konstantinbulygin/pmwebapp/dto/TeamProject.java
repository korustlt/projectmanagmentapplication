package com.konstantinbulygin.pmwebapp.dto;

/**
 * Интерфейс для получения пользователей и проектов в которых они участвуют
 */
public interface TeamProject {

    /**
     * Получение имени пользователя
     */
    String getTeamName();

    /**
     * Получение имени пользователя
     */
    long getTeamId();

    int getProjectCount();

}
