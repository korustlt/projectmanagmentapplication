package com.konstantinbulygin.pmwebapp.dto;

/**
 * Интерфейс для отображения данных о проектах и их статусов
 */
public interface ChartData {

    /**
     * Получение названия статуса
     */
    String getLabel();

    /**
     * Получение значения статуса
     */
    long getValue();
}
