package com.konstantinbulygin.pmwebapp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Основной метод для запуска приложения
 */
@SpringBootApplication
public class PmWebAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(PmWebAppApplication.class, args);
    }
}
