package com.konstantinbulygin.pmwebapp.entities;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * Класс представляет сущность пользователя
 */
@Entity
@Table(name = "user_accounts")
public class UserAccount {

    /**
     * Поле id пользователя
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long userId;

    /**
     * Поле логин пользователя
     */
    @NotBlank(message = "")
    @Size(min = 3, max = 25, message = "Логин должно быть длинной не менее 3 символов")
    @Column(name = "user_login")
    private String userLogin;

    /**
     * Поле имя пользователя
     */
    @NotBlank(message = "")
    @Size(min = 3, max = 25, message = "Имя должно быть длинной не менее 3 символов")
    @Column(name = "first_name")
    private String firstName;

    /**
     * Поле фамилия пользователя
     */
    @NotBlank(message = "")
    @Size(min = 3, max = 25, message = "Фамилия должна быть длинной не менее 3 символов")
    @Column(name = "last_name")
    private String lastName;

    /**
     * Поле почта пользователя
     */
    @NotBlank(message = "Введите корректный адрес электронной почты")
    @Email
    @Column(name = "email")
    private String email;

    /**
     * Поле пароль пользователя
     */
    @Size(min = 4, message = "Пароль должен быть длинной не менее 4 символов")
    @NotNull
    @Column(name = "password")
    private String password;

    /**
     * Поле подтверждение пароля пользователя
     */
    @Transient
    private String confirmPassword;

    /**
     * Поле дата рождения пользователя
     */
    @NotNull(message = "Введите корректную дату в формате гггг-мм-дд")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dob")
    private LocalDate userDob;

    /**
     * Поле название роли пользователя
     */
    @Column(name = "role")
    private String role = "ROLE_USER";

    /**
     * Поле ролей пользователя
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "users_authorities",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))
    @NotNull(message = "Выберите одного или несколько")
    private List<Authority> authorities;

    /**
     * Поле команд пользователя
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "team_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private List<Team> teams;

    /**
     * Конструктор без параметров
     */
    public UserAccount() {
    }

    /**
     * Конструктор с параметрами
     */
    public UserAccount(long userId, String userLogin, String firstName, String lastName, String email, String password, String confirmPassword, LocalDate userDob, String role, List<Authority> authorities, List<Project> projects, List<Team> teams) {
        this.userId = userId;
        this.userLogin = userLogin;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.userDob = userDob;
        this.role = role;
        this.authorities = authorities;
        this.teams = teams;
    }

    /**
     * Конструктор с параметрами
     */
    public UserAccount(String userLogin, String firstName, String lastName, String email, String password, String confirmPassword, LocalDate userDob, String role, List<Authority> authorities, List<Project> projects, List<Team> teams) {
        this.userLogin = userLogin;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.userDob = userDob;
        this.role = role;
        this.authorities = authorities;
        this.teams = teams;
    }

    /* геттеры и сеттеры */
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public LocalDate getUserDob() {
        return userDob;
    }

    public void setUserDob(LocalDate userDob) {
        this.userDob = userDob;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }


    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
