package com.konstantinbulygin.pmwebapp.entities;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

/**
 * Класс представляет сущность статуса
 */
@Entity
@Table(name = "statuses")
public class TaskStatus {

    /**
     * Поле id роли
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long statusId;

    /**
     * Поле название статуса
     */
    @NotBlank(message = "")
    @Size(min = 4, max = 15, message = "Статус задачи должен быть не менее 4 и не более 15")
    private String statusName;

    /**
     * Конструктор без параметров
     */
    public TaskStatus() {
    }

    /**
     * Конструктор с параметрами
     */
    public TaskStatus(long statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    /* геттеры и сеттеры */
    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
