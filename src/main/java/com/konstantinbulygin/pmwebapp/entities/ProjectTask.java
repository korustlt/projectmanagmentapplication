package com.konstantinbulygin.pmwebapp.entities;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


/**
 * Класс представляет сущность задачи для проекта
 */
@Entity
@Table(name = "tasks")
public class ProjectTask {

    /**
     * Поле id задачи
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long taskId;

    /**
     * Поле id проекта
     */
    @NotNull
    private long projectId;

    /**
     * Поле описание задачи
     */
    @NotBlank(message = "")
    @Size(min = 5, max = 255, message = "Описание задачи должно быть не менее 5 символов")
    private String description;

    /**
     * Поле id команды
     */
    @NotNull(message = "Описание задачи должно быть не менее 5 символов")
    private long teamId;

    /**
     * Поле id статуса
     */
    @NotNull(message = "Описание задачи должно быть не менее 5 символов")
    private long statusId;

    /**
     * Конструктор без параметров
     */
    public ProjectTask() {
    }

    /**
     * Конструктор с параметрами
     */
    public ProjectTask(long taskId, long projectId, String description, long teamId, long statusId) {
        this.taskId = taskId;
        this.projectId = projectId;
        this.description = description;
        this.teamId = teamId;
        this.statusId = statusId;
    }

    /**
     * Конструктор с параметрами
     */
    public ProjectTask(long projectId, String description, long teamId, long statusId) {
        this.projectId = projectId;
        this.description = description;
        this.teamId = teamId;
        this.statusId = statusId;
    }

    /* геттеры и сеттеры */
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
}
