package com.konstantinbulygin.pmwebapp.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * Класс представляет сущность проекта
 */
@Entity
@Table(name = "projects")
public class Project {

    /**
     * Поле id проекта
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long projectId;

    /**
     * Поле название проекта
     */
    @Size(min = 3, message = "Название проекта должно быть не менее 3 символов")
    private String projectName;

    /**
     * Поле id статуса
     */
    @NotNull(message = "Выберите статус")
    private long statusId;

    /**
     * Поле описание проекта
     */
    @Size(min = 5, max = 255, message = "Описание проекта должно быть не менее 5 символов")
    private String description;

    /**
     * Поле даты начала проекта
     */
    @NotNull(message = "Введите корректную дату в формате гггг-мм-дд")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    /**
     * Поле даты конца проекта
     */
    @NotNull(message = "Введите корректную дату в формате гггг-мм-дд")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    /**
     * Поле пользователей
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "project_team",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private List<Team> teams;

    /**
     * Конструктор без параметров
     */
    public Project() {
    }

    /**
     * Конструктор с параметрами
     */
    public Project(long projectId, String projectName, long statusId, String description, LocalDate startDate, LocalDate endDate, List<Team> teams) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.statusId = statusId;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teams = teams;
    }

    /**
     * Конструктор с параметрами
     */
    public Project(String projectName, long statusId, String description, LocalDate startDate, LocalDate endDate, List<Team> teams) {
        this.projectName = projectName;
        this.statusId = statusId;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teams = teams;
    }

    /* геттеры и сеттеры */
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
