package com.konstantinbulygin.pmwebapp.entities;

import jakarta.persistence.*;


/**
 * Класс представляет сущность роли в базе данных
 */
@Entity
@Table(name = "authorities")
public class Authority {

    /**
     * Поле id роли
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authority_id")
    private Integer authorityId;

    /**
     * Поле название роли
     */
    @Column(name = "authority_name")
    private String authorityName;

    /**
     * Конструктор без параметров
     */
    public Authority() {
    }

    /**
     * Конструктор с параметрами
     */
    public Authority(Integer authorityId, String authorityName) {
        this.authorityId = authorityId;
        this.authorityName = authorityName;
    }

    /**
     * Конструктор с параметрами
     */
    public Authority(String authorityName) {
        this.authorityName = authorityName;
    }

    /* геттеры и сеттеры */
    public Integer getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Integer authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }
}