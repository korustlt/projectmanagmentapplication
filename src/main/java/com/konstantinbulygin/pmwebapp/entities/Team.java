package com.konstantinbulygin.pmwebapp.entities;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

/**
 * Класс представляет сущность команды
 */
@Entity
@Table(name = "teams")
public class Team {

    /**
     * Поле id команды
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long teamId;

    /**
     * Поле название команды
     */
    @NotBlank(message = "")
    @Size(min = 3, max = 255, message = "Название команды должен быть не менее 3 символов")
    private String teamName;

    /**
     * Поле пользователей команды
     */
    //@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "team_user",
            joinColumns = @JoinColumn(name = "team_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    @NotNull(message = "Выберите одного или несколько пользователей")
    private List<UserAccount> teamUsers;

    /**
     * Конструктор без параметров
     */
    public Team() {
    }

    /**
     * Конструктор с параметрами
     */
    public Team(long teamId, String teamName, List<UserAccount> teamUsers) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.teamUsers = teamUsers;
    }

    /* геттеры и сеттеры */
    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<UserAccount> getTeamUsers() {
        return teamUsers;
    }

    public void setTeamUsers(List<UserAccount> teamUsers) {
        this.teamUsers = teamUsers;
    }
}
