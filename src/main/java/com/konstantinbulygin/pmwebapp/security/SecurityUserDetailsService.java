package com.konstantinbulygin.pmwebapp.security;

import com.konstantinbulygin.pmwebapp.entities.Authority;
import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import com.konstantinbulygin.pmwebapp.services.UserAccountDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Сервис безопасности для авторизации пользователей
 */
@Service
@SuppressWarnings("unused")
public class SecurityUserDetailsService implements UserDetailsService {

    /**
     * Поле логера
     */
    private final Logger logger = LoggerFactory.getLogger(SecurityUserDetailsService.class);

    /**
     * Поле репозитория пользователей
     */
    private final UserAccountDaoImpl userAccountDao;

    /**
     * Внедрение зависимостей через конструктор
     */
    public SecurityUserDetailsService(UserAccountDaoImpl userAccountRepository) {
        this.userAccountDao = userAccountRepository;
    }

    /**
     * Переопределенный метод loadUserByUsername() из UserDetailsService.
     * Возвращает информацию об авторизованном пользователе.
     */
    @Override
    public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {

        // получение пользователя из базы данных по логину
        Optional<UserAccount> user = userAccountDao.findByUserLogin(userLogin);
        // проверка условия, если пользовать найден
        if (user.isPresent()) {
            // проверка условия, совпадает ли логин пользователя с логином который ввели
            if (!user.get().getUserLogin().equals(userLogin)) {
                // логирование ошибки авторизации
                logger.error("Пользователя с таким логином нет: {}", userLogin);
                // выброс ошибки, что пользователь не найден
                throw new UsernameNotFoundException("Неправильные данные!");
            }
            // создание набора для допущенных ролей
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            // получение роли для пользователя
            Authority authority = userAccountDao.findAuthorities(user.get().getRole());
            // установка роли в набор допущенных ролей
            grantedAuthorities.add(new SimpleGrantedAuthority(authority.getAuthorityName()));
            // возвращение авторизованного пользователя
            return new SecurityUser(user.get());
        } else {
            // выброс ошибки, что пользователь не найден
            throw new UsernameNotFoundException(userLogin);
        }
    }
}
