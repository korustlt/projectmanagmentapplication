package com.konstantinbulygin.pmwebapp.security;

import com.konstantinbulygin.pmwebapp.entities.UserAccount;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;


/**
 * Класс безопасности аккаунта пользователя
 */
public class SecurityUser implements UserDetails {

    /**
     * Поле пользователя
     */
    private final UserAccount userAccount;

    /**
     * Конструктор с параметрами
     */
    public SecurityUser(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    /**
     * Переопределенный метод getUsername() из UserDetails.
     * Возвращает логин пользователя
     */
    @Override
    public String getUsername() {
        return userAccount.getUserLogin();
    }

    /**
     * Переопределенный метод getPassword() из UserDetails.
     * Возвращает пароль пользователя
     */
    @Override
    public String getPassword() {
        return userAccount.getPassword();
    }

    /**
     * Переопределенный метод getAuthorities() из UserDetails.
     * Возвращает роль пользователя
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays
                .stream(userAccount.getRole().split(","))
                .map(SimpleGrantedAuthority::new).toList();

    }

    /**
     * Переопределенный метод isAccountNonExpired() из UserDetails.
     * Возвращает булево значение, истек срок аккаунта пользователя или нет
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Переопределенный метод isAccountNonLocked() из UserDetails.
     * Возвращает булево значение, заблокирован аккаунт или нет
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Переопределенный метод isCredentialsNonExpired() из UserDetails.
     * Возвращает булево значение, истек срок пароля и логина или нет
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Переопределенный метод isEnabled() из UserDetails.
     * Возвращает булево значение, доступен аккаунт или нет
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
