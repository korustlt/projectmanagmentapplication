package com.konstantinbulygin.pmwebapp.security;

import jakarta.servlet.DispatcherType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


/**
 * Класс конфигурации безопасности
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@SuppressWarnings("unused")
public class SecurityConfig {

    /**
     * Поле класса безопасности аккаунта пользователя
     */
    private final SecurityUserDetailsService securityUserDetailsService;

    /**
     * Конструктор с параметрами
     */
    public SecurityConfig(SecurityUserDetailsService securityUserDetailsService) {
        this.securityUserDetailsService = securityUserDetailsService;
    }

    /**
     * Возвращает экземпляр объекта кодировщика паролей
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Возвращает экземпляр объекта SecurityFilterChain, цепочка фильтров для обработки запросов
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        // установка заголовков
        http.headers(headers -> headers.frameOptions(Customizer.withDefaults()))
                // настройка фильтра авторизации для разных путей запросов
                .authorizeHttpRequests(authorize -> authorize
                        .dispatcherTypeMatchers(DispatcherType.FORWARD, DispatcherType.ERROR).permitAll()
                        .requestMatchers("/projects/new").hasRole("ADMIN")
                        .requestMatchers("/projects/save").hasRole("ADMIN")
                        .requestMatchers("/projects/edit/{id}").hasRole("ADMIN")
                        .requestMatchers("/projects/delete/{id}").hasRole("ADMIN")
                        .requestMatchers("/employees/save").hasRole("ADMIN")
                        .requestMatchers("/employees/new").hasRole("ADMIN")
                        .requestMatchers("/employees/edit/{id}").hasRole("ADMIN")
                        .requestMatchers("/employees/delete/{id}").hasRole("ADMIN")
                        .requestMatchers("/admin/save").hasRole("ADMIN")
                        .requestMatchers("/admin/team/save").hasRole("ADMIN")
                        .requestMatchers("/admin/status/save").hasRole("ADMIN")
                        .requestMatchers("/", "/**").permitAll()
                        .anyRequest().authenticated()
                )
                // установка данных авторизованного пользователя
                .userDetailsService(securityUserDetailsService)
                .httpBasic(Customizer.withDefaults())
                .exceptionHandling(Customizer.withDefaults())
                // настройка формы для входа
                .formLogin(
                        form -> {
                            form.loginPage("/login").permitAll();
                            form.defaultSuccessUrl("/");
                            form.failureUrl("/login?loginError=true");
                        }
                )
                // настройка формы для выхода
                .logout(logout -> {
                    logout.logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
                    logout.logoutSuccessUrl("/");
                    logout.deleteCookies("JSESSIONID");
                    logout.invalidateHttpSession(true);
                });
        http
                // настройка сессии
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.ALWAYS))
                // настройка максимума сессий для одного пользователя
                .sessionManagement(session -> session.maximumSessions(2));
        http.sessionManagement(session -> session.sessionFixation().migrateSession());
        // возвращение настроенного фильтра SecurityFilterChain
        return http.build();
    }
}
